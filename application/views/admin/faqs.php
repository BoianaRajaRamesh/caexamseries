<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>admin/dashboard">Dashboard</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <!-- table-responsive -->
                    <div class="">
                        <table id="myTable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Question</th>
                                    <th>Answer</th>
                                    <th>FAQ Page</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($list) && (count($list) > 0)) {
                                    $i = 1;
                                    foreach ($list as $item) {
                                        ?>
                                        <tr>
                                            <td><?= $i ?></td>
                                            <td><?= $item->question ?></td>
                                            <td><?= $item->answer ?></td>
                                            <td class="text-uppercase"><?= $item->faq_page ?></td>
                                            <td class="text-capitalize"><?= $item->status ?></td>
                                            <td>
                                                <a href="<?= base_url() ?>admin/faqs/update/<?= $item->id ?>" class="btn btn-sm btn-circle btn-success"><i class="fa fa-edit"></i></a>
                                                <button type="button" class="btn btn-sm btn-circle btn-danger delete_item" value="<?= $item->id ?>"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $form_title; ?> <?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <form action="" method="post" enctype="multipart/form-data">

                        <div class="form-group">
                            <label>Question</label>
                            <input class="form-control" type="text" placeholder="Question" name="question" value="<?= (isset($details->question)) ? $details->question : '' ?>">
                            <?php echo form_error('question', '<div class="error">', '</div>'); ?>
                        </div>

                        <div class="form-group">
                            <label>Answer</label>
                            <textarea class="form-control" name="answer"><?= (isset($details->answer)) ? $details->answer : '' ?></textarea>
                            <?php echo form_error('answer', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-success">
                                        <input name="status" id="radio1" value="active" type="radio" <?= (isset($details->status) && ($details->status == 'active')) ? 'checked' : '' ?> required>
                                        <label for="radio1">Active</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-danger">
                                        <input name="status" id="radio2" value="inactive" type="radio" <?= (isset($details->status) && ($details->status == 'inactive')) ? 'checked' : '' ?> required>
                                        <label for="radio2">Inactive</label>
                                    </div>
                                </label>
                            </div>
                            <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="form-group">
                            <label class="control-label">FAQ Page</label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-success">
                                        <input name="faq_page" id="radio11" value="course" type="radio" <?= (isset($details->faq_page) && ($details->faq_page == 'course')) ? 'checked' : '' ?> required>
                                        <label for="radio11">Course</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-danger">
                                        <input name="faq_page" id="radio21" value="pg" type="radio" <?= (isset($details->faq_page) && ($details->faq_page == 'pg')) ? 'checked' : '' ?> required>
                                        <label for="radio21">PG</label>
                                    </div>
                                </label>
                            </div>
                            <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                        </div>
                        <button type="submit" class="btn btn-rounded px-20 btn-info waves-effect waves-light m-r-10" name="submit" value="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('admin/includes/footer');
?>
<script type="text/javascript">
    $('#text').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9 ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $(document).ready(function () {
        $('#myTable').DataTable();
        //Delete Confirmation Script starts here
        $(document).on("click", '.delete_item', function () {
            var del_id = $(this).val();
            swal({
                title: "Are you sure?",
                text: "You want to Delete entire data related to this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                window.location = "<?= base_url() ?>admin/faqs/delete/" + del_id;
            });
        });
        //Delete Confirmation Script ends here
    });
</script>