<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Plans_model extends CI_Model {

    private $table = "plans";

    function get_data() {
        $this->db->select('plans.*, exams.title as exam_title, courses.course');
        $this->db->join("courses", "courses.id = plans.course_id");
        $this->db->join("exams", "exams.id = plans.exam_id", "Left");
        $this->db->where("plans.status !=", 'deleted');
        $this->db->order_by("plans.id", "desc");
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
    }

    function add($data, $subjects_list, $points) {
        $this->db->trans_start();

        $this->db->set($data);
        $this->db->insert($this->table);
        $id = $this->db->insert_id();

        foreach ($subjects_list as $row) {
            $subjects_data = array(
                'plan_id' => $id,
                'subject_id' => $row,
                'created_at' => time(),
            );
            $this->db->set($subjects_data);
            $this->db->insert('plan_wise_subjects');
        }

        foreach ($points as $row) {
            if ($row != '') {
                $subjects_data = array(
                    'plan_id' => $id,
                    'description' => $row,
                    'created_at' => time(),
                );
                $this->db->set($subjects_data);
                $this->db->insert('plan_wise_points');
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    function update($id, $data, $subjects_list, $points) {
        $this->db->trans_start();

        $this->db->set($data);
        $this->db->where('id', $id);
        $this->db->update($this->table);

        $this->db->where('plan_id', $id);
        $this->db->delete('plan_wise_subjects');

        foreach ($subjects_list as $row) {
            $subjects_data = array(
                'plan_id' => $id,
                'subject_id' => $row,
                'created_at' => time(),
            );
            $this->db->set($subjects_data);
            $this->db->insert('plan_wise_subjects');
        }

        $this->db->where('plan_id', $id);
        $this->db->delete('plan_wise_points');

        foreach ($points as $row) {
            if ($row != '') {
                $subjects_data = array(
                    'plan_id' => $id,
                    'description' => $row,
                    'created_at' => time(),
                );
                $this->db->set($subjects_data);
                $this->db->insert('plan_wise_points');
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return FALSE;
        } else {
            $this->db->trans_commit();
            return TRUE;
        }
    }

    function get_details($id) {
        $this->db->where('id', $id);
        $data = $this->db->get($this->table)->row();
        $data->exam_title = $this->db->where('id', $data->exam_id)->get('exams')->row()->title;
        $data->plan_wise_subjects = $this->db->where('plan_id', $id)->get('plan_wise_subjects')->result();
        $data->plan_wise_points = $this->db->where('plan_id', $id)->get('plan_wise_points')->result();
        return $data;
    }

    function delete($id) {
        $this->db->set('status', 'deleted');
        $this->db->where('id', $id);
        if ($this->db->update('plans')) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
