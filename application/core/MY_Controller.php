<?php

header("access-control-allow-origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Access-Control-Allow-Headers: Content-Type");
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->helper('form');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper("file");
        define('FILE_PATH', base_url() . "/uploads/");
        define('DEFAULT_IMAGE', base_url() . "/uploads/default_image.png");
    }

    function get_designation_id() {
        return $this->session->userdata('designation_id');
    }

//    function check_user_access($unique_name) {
//        $designation_id = $this->get_designation_id();
//        $count = $this->my_db->get_records_count("CALL CheckUserAccess('" . $unique_name . "', $designation_id)");
//        if ($count == 1) {
//            return true;
//        } else {
//            redirect('admin/common/error_page', 'refresh');
//            die();
//        }
//    }
//    function user_access($unique_name) {
//        $designation_id = $this->get_designation_id();
//        return $this->my_db->get_records_count("CALL CheckUserAccess('" . $unique_name . "', $designation_id)");
//    }

    function get_user_menu() {
        $designation_id = $this->get_designation_id();
        if ($designation_id == 1) {
            return $this->common_model->get_admin_menu();
        } else {

        }
    }

    function admin_view($view_file) {
        if ($this->session->userdata('user_type') == 'admin') {
            $this->data['admin_details'] = $this->common_model->admin_details();
        } else {
            $this->data['staff_details'] = $this->common_model->staff_details();
        }
        $this->data['menu'] = $this->get_user_menu();
        $this->data['settings'] = $this->common_model->get_single_data('settings', 1);
        $this->load->view('admin/includes/header', $this->data);
        $this->load->view('admin/' . $view_file, $this->data);
    }

    function web_view($view_file) {
        $this->data['site_settings'] = $this->common_model->get_single_data('settings', 1);
        $this->data['courses_list'] = $this->course_model->get_active_data();
        $this->data['schedules'] = $this->course_model->get_schedules_data();
//        echo "<pre>";
//        print_r($this->data['schedules']);
//        die;
        $this->load->view('website/includes/header', $this->data);
        $this->load->view('website/' . $view_file, $this->data);
    }

    public function login_required() {
        if (!$this->session->userdata('validated')) {
            redirect(base_url('admin'), 'refresh');
        } else {
            return true;
        }
    }

    public function user_login_required() {
        if (!$this->session->userdata('web_user_id')) {
            redirect(base_url('login'), 'refresh');
        } else {
            return true;
        }
    }

    public function file_upload($file) {
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = 'jpg|png|pdf|svg';
        $config['max_size'] = 5096;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['remove_spaces'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($file)) {
            $err = $this->upload->display_errors();
            $this->session->set_flashdata('error_message', '"' . $err . '","Failed!"');
            echo '<script>window.history.go(-1)</script>';
            die;
        } else {
            return $this->upload->data('file_name');
        }
    }

    public function file_upload_with_specific_path($file, $path) {
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'jpg|png|pdf|svg';
        $config['max_size'] = 10200; //currently 10 MB //5096 5MB
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        $config['remove_spaces'] = true;
        $config['encrypt_name'] = TRUE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($file)) {
            $err = $this->upload->display_errors();
            $this->session->set_flashdata('error_message', '"' . $err . '","Failed!"');
            echo '<script>window.history.go(-1)</script>';
            die;
        } else {
            return $this->upload->data('file_name');
        }
    }

    public function send_mail($subject, $message, $to_email) {
        $settings = $this->common_model->get_single_data('settings', 1);
        $this->load->library('email');
        $config = array();
        $config['useragent'] = "CodeIgniter";
        $config['protocol'] = "smtp";
        $config['smtp_host'] = "ssl://sg4.fcomet.com";
        $config['smtp_user'] = "twinlity@demo.org.in";
        $config['smtp_pass'] = "vizag@123";
        $config['smtp_port'] = "465";
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['wordwrap'] = true;
        $this->email->initialize($config);

        $system_name = $settings->title;
        $from = $settings->support_mail;
        $this->email->from($from, $system_name);

        $this->email->to($to_email);
        $this->email->subject($subject);
        $this->email->message($message);
        $send = $this->email->send();
//        echo $this->email->print_debugger();die();
        return TRUE;
    }

    function common_pagination($url, $total_count, $per_page) {
        $config['base_url'] = $url;
        $config['total_rows'] = $total_count;
        $config['per_page'] = $per_page;
        $config['page_query_string'] = true;
        $config['num_links'] = 10;
        $config['full_tag_open'] = "<ul class='pagination'>";
        $config['full_tag_close'] = '</ul>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';


        $config['prev_link'] = '<span aria-hidden="true">&laquo;</span>';
        $config['prev_tag_open'] = '<li class="button grey">';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = '<span aria-hidden="true">&raquo;</span>';
        $config['next_tag_open'] = '<li class="button grey">';
        $config['next_tag_close'] = '</li>';

//        $start = ($this->input->get_post('per_page')) ? $this->input->get_post('per_page') : 0;
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
    }

}
