<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>admin/dashboard">Dashboard</a>
            </li>
            <li>
                <a href="<?= base_url(); ?>admin/blog">Blog</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div>
            <div class="ibox float-e-margins">
                <!--                <div class="ibox-title">
                                    <h5><?= $page_title; ?></h5>
                                </div>-->
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="" enctype="multipart/form-data" autocomplete="off">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Title</label>
                                <div class="col-sm-12">
                                    <input type="text" name="title" class="form-control" required="" value="<?php if (isset($details)) echo $details->title ?>" placeholder="Title">
                                </div>
                                <?php echo form_error('title', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Blog Banner</label>
                                <div class="col-sm-12">
                                    <input type="file" name="image" class="form-control">
                                    <?php if (isset($details)) { ?>
                                        <span>Only Image formats are available <b><a target="_blank" href="<?= base_url($details->image); ?>">Previous Image</a></b></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-12 ">Description</label>
                                <div class="col-sm-12">
                                    <textarea class="form-control" placeholder="Description" id="description" name="description"><?php if (isset($details)) echo $details->description ?></textarea>
                                </div>
                                <?php echo form_error('description', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label">Popular</label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-success">
                                        <input name="popular" id="radio11" value="yes" type="radio" <?= (isset($details->popular) && ($details->popular == 'yes')) ? 'checked' : '' ?> required>
                                        <label for="radio11">Yes</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-danger">
                                        <input name="popular" id="radio21" value="no" type="radio" <?= (isset($details->popular) && ($details->popular == 'no')) ? 'checked' : '' ?> required>
                                        <label for="radio21">No</label>
                                    </div>
                                </label>
                            </div>
                            <?php echo form_error('popular', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label class="control-label">Status</label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-success">
                                        <input name="status" id="radio1" value="active" type="radio" <?= (isset($details->status) && ($details->status == 'active')) ? 'checked' : '' ?> required>
                                        <label for="radio1">Active</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-danger">
                                        <input name="status" id="radio2" value="inactive" type="radio" <?= (isset($details->status) && ($details->status == 'inactive')) ? 'checked' : '' ?> required>
                                        <label for="radio2">Inactive</label>
                                    </div>
                                </label>
                            </div>
                            <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="col-md-2">
                            <label class="col-sm-12"> &nbsp; </label>
                            <input type="submit" name="submit" class="btn btn-primary btn-block" value="<?= $page_title; ?>">
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/includes/footer'); ?>
<script type="text/javascript">
    CKEDITOR.replace("description");
</script>