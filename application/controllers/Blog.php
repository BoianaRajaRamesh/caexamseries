<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends MY_Controller {

    private $model = "Blog_model";

    function __construct() {
        parent::__construct();
        $this->load->model("admin/" . $this->model);
    }

    public function index() {
        $this->data['page'] = 'Blog';
        $this->data['blogs_list'] = $this->{$this->model}->get_active_data();
        $this->data['popular_blogs_list'] = $this->{$this->model}->get_active_popular_data();
        $this->web_view('blog', $this->data);
    }

    function blog_view($id) {
        if (isset($id) || $id != '') {
            $this->data['page'] = 'Blog';
            $this->data['blogs_details'] = $this->{$this->model}->get_details($id);
            $this->data['popular_blogs_list'] = $this->{$this->model}->get_active_popular_data();
            $this->web_view('blog_view', $this->data);
        } else {
            redirect(base_url() . 'blog');
        }
    }

}
