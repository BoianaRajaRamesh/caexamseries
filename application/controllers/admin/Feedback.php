<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends MY_Controller {

    private $model = "Feedback_model";

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/' . $this->model);
        $this->data['unique_name'] = 'masters';
        $this->data['sub_unique_name'] = 'feedback';
    }

    function index() {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("link", "link", "required|is_unique[feedback.link]", array(
                'required' => 'link cannot be empty',
                "is_unique" => "link must be unique.",
            ));
            $this->form_validation->set_rules("status", "Status", "required", array(
                'required' => 'Status cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {

            } else {
                $data = array(
                    'link' => $this->input->post('link'),
                    'status' => $this->input->post('status'),
                    'created_at' => time(),
                );
                $res = $this->{$this->model}->add($data);
                if ($res) {
                    $this->session->set_flashdata('success_message', '"Success!","Added Successfully!"');
                    redirect(base_url() . 'admin/feedback');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'admin/feedback');
                }
            }
        }
        $this->data['page_title'] = 'Students Feedback';
        $this->data['form_title'] = 'Add';
        $this->data['list'] = $this->{$this->model}->get_data();
        $this->admin_view('feedback');
    }

    function update($id) {
        if ((isset($id)) && ($id != '')) {
            $this->data['details'] = $this->{$this->model}->get_details($id);

            if ($this->input->post('submit')) {
                if ($this->data['details']->link == $this->input->post('link')) {
                    $this->form_validation->set_rules("link", "link", "required", array(
                        'required' => 'link cannot be empty',
                    ));
                } else {
                    $this->form_validation->set_rules("link", "link", "required|is_unique[feedback.link]", array(
                        'required' => 'link cannot be empty',
                        "is_unique" => "link must be unique.",
                    ));
                }
                $this->form_validation->set_rules("status", "Status", "required", array(
                    'required' => 'Status cannot be empty',
                ));
                if ($this->form_validation->run() == FALSE) {

                } else {
                    $data = array(
                        'link' => $this->input->post('link'),
                        'status' => $this->input->post('status'),
                        'updated_at' => time(),
                    );
                    $res = $this->{$this->model}->update($id, $data);
                    if ($res) {
                        $this->session->set_flashdata('success_message', '"Success!","Updated Successfully!"');
                        redirect(base_url() . 'admin/feedback');
                    } else {
                        $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                        redirect(base_url() . 'admin/feedback/update/' . $id);
                    }
                }
            }
            $this->data['page_title'] = 'Students Feedback';
            $this->data['form_title'] = 'Update';
            $this->data['list'] = $this->{$this->model}->get_data();
            $this->admin_view('feedback');
        }
    }

    function delete($id) {
        if ((isset($id)) && ($id != '')) {
            if ($this->{$this->model}->delete($id)) {
                $this->session->set_flashdata('success_message', "'Deleted Successfully', 'Success'");
                redirect(base_url() . 'admin/feedback');
            } else {
                $this->session->set_flashdata('error_message', "'Please Try Again', 'Error'");
                redirect(base_url() . 'admin/feedback');
            }
        } else {
            redirect(base_url() . 'admin/feedback');
        }
    }

}
