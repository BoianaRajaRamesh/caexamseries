<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Banners_model extends CI_Model {

    function get_banners() {
//        $this->db->order_by("id", "desc");
        $this->db->order_by("priority", "asc");
        $data = $this->db->get('banners');
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
    }

    function get_active_banners() {
        $this->db->where("status", 1);
        $this->db->order_by("priority", "asc");
        $data = $this->db->get('banners');
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
    }

    function get_banners_details($parameters) {
        $this->db->where('id', $parameters['id']);
        $data = $this->db->get('banners');
        if ($data->num_rows() > 0) {
            $result = $data->row();
            return $result;
        }
    }

    function add($data) {
        $this->db->set($data);
        $this->db->set('created_at', time());
        $this->db->insert('banners');
        if ($this->db->insert_id()) {
            return $this->db->insert_id();
        }
    }

    function update($parameters, $data) {
        $this->db->set($data);
        $this->db->set('updated_at', time());
        $this->db->where('id', $parameters['id']);
        if ($this->db->update('banners')) {
            return true;
        }
    }

    function delete($parameters) {
        $this->db->where('id', $parameters['id']);
        $this->db->delete('banners');
        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

    function get_app_banners() {
        $this->db->where('status', 1);
        $this->db->order_by("priority", "asc");
        $data = $this->db->get('banners');
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        } else {
            return [];
        }
    }

}
