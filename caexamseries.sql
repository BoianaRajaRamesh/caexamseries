-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 09, 2021 at 12:50 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `caexamseries`
--

-- --------------------------------------------------------

--
-- Table structure for table `access_privilege`
--

CREATE TABLE `access_privilege` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `privilege_id` int(10) UNSIGNED NOT NULL,
  `designation` int(10) UNSIGNED NOT NULL,
  `created_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `salt` int(11) NOT NULL,
  `display_name` varchar(150) NOT NULL,
  `mobile_number` varchar(15) NOT NULL,
  `email` varchar(150) NOT NULL,
  `role` int(11) DEFAULT NULL,
  `image` varchar(250) DEFAULT NULL,
  `status` enum('1','0') NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`, `salt`, `display_name`, `mobile_number`, `email`, `role`, `image`, `status`, `created_date`, `updated_date`) VALUES
(1, 'admin', '28ef62ec7e97889bb2ba61101e8718f8', 53440, 'Super Admin', '8895568688', 'raja@thecolourmoon.com', 1, 'caexamseries_favicon1.png', '1', '2020-01-08 11:31:00', '2021-01-13 14:01:06');

-- --------------------------------------------------------

--
-- Table structure for table `admin_logs`
--

CREATE TABLE `admin_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_logs`
--

INSERT INTO `admin_logs` (`id`, `admin_id`, `ip_address`, `created_date`, `updated_date`) VALUES
(26, 1, '::1', '2021-01-19 09:58:48', NULL),
(27, 1, '::1', '2021-01-20 09:35:28', NULL),
(28, 1, '::1', '2021-01-20 18:12:54', '2021-01-20 18:17:19'),
(29, 1, '::1', '2021-01-21 22:55:50', NULL),
(30, 1, '::1', '2021-01-30 10:05:36', NULL),
(31, 1, '::1', '2021-02-01 10:02:31', NULL),
(32, 1, '::1', '2021-02-02 10:02:00', '2021-02-02 10:56:47'),
(33, 1, '::1', '2021-02-05 16:42:43', NULL),
(34, 1, '::1', '2021-02-06 12:35:39', NULL),
(35, 1, '::1', '2021-02-08 15:48:52', NULL),
(36, 1, '::1', '2021-02-09 16:00:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(11) NOT NULL,
  `videos_size` int(11) NOT NULL COMMENT 'count of all videos size must be less than this value',
  `created_date` datetime NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_settings`
--

INSERT INTO `app_settings` (`id`, `videos_size`, `created_date`, `updated_date`) VALUES
(1, 200, '2020-06-04 12:12:51', '2020-06-04 12:12:51');

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` bigint(20) NOT NULL,
  `image` varchar(255) NOT NULL,
  `mobile_slider` varchar(250) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 1,
  `priority` int(11) NOT NULL,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`id`, `image`, `mobile_slider`, `status`, `priority`, `created_at`, `updated_at`) VALUES
(1, 'uploads/1610531241.jpg', 'uploads/1610532869.jpg', 1, 1, '1586773225', '1611250293');

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `title` varchar(1000) NOT NULL,
  `description` text NOT NULL,
  `popular` enum('yes','no') NOT NULL DEFAULT 'no',
  `status` enum('active','inactive') NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `image`, `title`, `description`, `popular`, `status`, `created_at`, `updated_at`) VALUES
(2, 'uploads/blog/01534Blog-Cover-15-mins1.jpg', 'How to Utilize GOLDEN 15 Minutes Effectively Before Exam', '<p>Hello Future Chartered Accountant<br />\r\n<br />\r\nToday&rsquo;s Topic &ndash; Golden 15 Minutes of time before the exam and how to utilize it effectively?<br />\r\n<br />\r\nMost of the students try to solve a particular problem in those 15 minutes to cover the time but it&#39;s the Main blunder that no one should do? Then what is the ideal purpose to utilize those 15 minutes?<br />\r\n<br />\r\nHere are the tips to utilize effectively.</p>\r\n\r\n<h2>Never solve a problem</h2>\r\n\r\n<p>Don&rsquo;t be in a hurry to solve the problem one among it and give additional 15 minutes to your exam, it&rsquo;s just blunder to do so. Never ever do that.</p>\r\n\r\n<h2>Judge the Question Paper</h2>\r\n\r\n<p>Yes, what you read was true, judge the question paper whether it was tough or easy. just by going through the model of questions we can easily judge whether question paper was tough or not, lengthy or not, etc.</p>\r\n\r\n<h2>Prepare Your Mindset accordingly</h2>\r\n\r\n<p>After judging the question Paper, Prepare your mindset according to it &ndash; if its easy raise your confidence levels and if its tough prepare your mindset to make sure that you are going to crack it at any cost. It should be done very fast and till this it should be below 5 Minutes.</p>\r\n', 'no', 'active', '1610541296', '1611250002'),
(3, 'uploads/blog/9ebac1a770666baf06793ecc88cbe506.jpg', 'How to Utilize GOLDEN 15 Minutes Effectively Before Exam', '<p>Hello Future Chartered Accountant<br />\r\n<br />\r\nToday&rsquo;s Topic &ndash; Golden 15 Minutes of time before the exam and how to utilize it effectively?<br />\r\n<br />\r\nMost of the students try to solve a particular problem in those 15 minutes to cover the time but it&#39;s the Main blunder that no one should do? Then what is the ideal purpose to utilize those 15 minutes?<br />\r\n<br />\r\nHere are the tips to utilize effectively.</p>\r\n', 'yes', 'active', '1610541296', '1612174880');

-- --------------------------------------------------------

--
-- Table structure for table `call_back_request`
--

CREATE TABLE `call_back_request` (
  `id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone_number` varchar(10) NOT NULL,
  `course` int(11) NOT NULL,
  `reason` varchar(150) NOT NULL,
  `feedback` varchar(1500) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `call_back_request`
--

INSERT INTO `call_back_request` (`id`, `name`, `email`, `phone_number`, `course`, `reason`, `feedback`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Raja Ramesh', 'raja@thecolourmoon.com', '8895568688', 1, 'To Know about CA Exam Series', 'Testing', 'active', '1612162751', NULL),
(2, 'admin testing', 'raja@thecolourmoon.com', '8895568688', 2, 'To Know about Payment Procedure', 'Lorem Ipsum is simply typesetting dummy text of the printing and typesetting industry. Lorem Ipsum text has been the industry\'s standard.', 'active', '1612169729', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `course` varchar(100) NOT NULL,
  `brochure` varchar(250) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `course`, `brochure`, `status`, `created_at`, `updated_at`) VALUES
(1, 'CA Foundation', 'uploads/courses/84140281589e8f18e258df42f55a5304.pdf', 'active', '1612156717', NULL),
(2, 'CA Inter', 'uploads/courses/8d57f6fd6fc5c31624677ad498889a41.pdf', 'active', '1612167691', NULL),
(3, 'CA Final', 'uploads/courses/da4b8679afe0f3a1263ad859603ca987.pdf', 'active', '1612167767', NULL),
(4, 'CA IPCC (Old)', 'uploads/courses/79a23f9671fc32bb8329d4679dba3ae3.pdf', 'active', '1612161265', NULL),
(5, 'CA Final (Old)', 'uploads/courses/3b1d764c3fa88d66799db88817355d52.pdf', 'active', '1612167932', NULL),
(7, 'Mock Exam', 'uploads/courses/03d4a30ef95035d1198c9bafaaed0fbb.pdf', 'active', '1612867374', NULL),
(8, 'Pass Guarantee Plan', 'uploads/courses/d8e13f3d5bfa73ac520617f8038e8f3d.pdf', 'active', '1612867406', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `coursewise_subjects`
--

CREATE TABLE `coursewise_subjects` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `created_at` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `exams`
--

CREATE TABLE `exams` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `exams`
--

INSERT INTO `exams` (`id`, `title`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Chapter Wise', 'Each Chapter = 1 Chapter Exam', 'active', '1612593949', NULL),
(2, 'Section Wise', '2-3 Chapters = 1 Section Exam', 'active', '1612593949', NULL),
(3, 'Unit Wise', '5-6 Chapters = 1 Unit Exam', 'active', '1612593949', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

CREATE TABLE `faqs` (
  `id` int(11) NOT NULL,
  `question` varchar(500) NOT NULL,
  `answer` varchar(1500) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `faq_page` enum('course','pg') NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `status`, `faq_page`, `created_at`, `updated_at`) VALUES
(1, 'Test', 'Testing\r\n', 'active', 'pg', '1610543843', '1612525785'),
(3, 'Test1', 'Testing1', 'active', 'course', '1612525798', '1612525827');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `link` varchar(250) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`id`, `link`, `status`, `created_at`, `updated_at`) VALUES
(2, 'lH57NtTWSFQ', 'active', '1611038624', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `feedback_from_contact_page`
--

CREATE TABLE `feedback_from_contact_page` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone_number` varchar(10) NOT NULL,
  `course` int(11) NOT NULL,
  `feedback` varchar(1500) NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL DEFAULT 'active',
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback_from_contact_page`
--

INSERT INTO `feedback_from_contact_page` (`id`, `name`, `email`, `phone_number`, `course`, `feedback`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Raja Ramesh', 'raja@thecolourmoon.com', '8895568688', 1, 'Testing', 'active', '1612164332', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `how_it_works`
--

CREATE TABLE `how_it_works` (
  `id` int(11) NOT NULL,
  `icon` varchar(25) NOT NULL,
  `title` varchar(500) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT 0,
  `status` enum('active','inactive') NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `how_it_works`
--

INSERT INTO `how_it_works` (`id`, `icon`, `title`, `priority`, `status`, `created_at`, `updated_at`) VALUES
(1, 'fal fa-download', '<p>Download<br />\r\nQuestion Paper</p>\r\n', 1, 'active', '1611040905', '1612870837'),
(3, 'fal fa-pen', '<p>Write Exam<br />On Paper</p>\r\n', 2, 'active', '1611045612', '1612870872'),
(4, 'fal fa-scanner-image', '<p>Scan The<br />Answer Paper</p>\r\n', 3, 'active', '1611045642', '1612870882'),
(5, 'fal fa-upload', '<p>Upload<br />Scanned Copy</p>\r\n', 4, 'active', '1611045663', '1612870891'),
(6, 'fal fa-check', '<p>Your Answers<br />will be Evaluated</p>\r\n', 5, 'active', '1611045695', '1612870901'),
(7, 'fal fa-analytics', '<p>Analyse &amp; Correct<br />Your Mistakes</p>\r\n', 6, 'active', '1611045717', '1612870864');

-- --------------------------------------------------------

--
-- Table structure for table `main_menu`
--

CREATE TABLE `main_menu` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(500) DEFAULT NULL,
  `unique_name` varchar(150) NOT NULL,
  `priority` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `main_menu`
--

INSERT INTO `main_menu` (`id`, `title`, `icon`, `url`, `unique_name`, `priority`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Dashboard', 'fa fa-dashboard', 'dashboard', 'dashboard', 1, 1, '1610532869', NULL),
(2, 'Masters', 'fa fa-gear', NULL, 'masters', 2, 1, '1610532869', NULL),
(3, 'Blog', 'fa fa-newspaper-o', 'blog', 'blog', 3, 1, '1610532869', NULL),
(4, 'Faq\'s', 'fa fa-question-circle', 'faqs', 'faqs', 4, 1, '1610532869', NULL),
(5, 'Courses', 'fa fa-graduation-cap', 'courses', 'courses', 2, 1, '1610532869', NULL),
(6, 'Support', 'fa fa-life-ring', NULL, 'support', 10, 1, '1610532869', NULL),
(7, 'Plans', 'fa fa-tasks', 'plans', 'plans', 3, 1, '1610532869', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

CREATE TABLE `plans` (
  `id` int(11) NOT NULL,
  `plan_title` varchar(250) NOT NULL,
  `actual_price` int(11) NOT NULL,
  `discount_price` int(11) NOT NULL,
  `exam_id` int(11) NOT NULL,
  `exam_mode` enum('Schedule','Un-Schedule','Other') NOT NULL,
  `schedule_pdf` varchar(250) NOT NULL,
  `subject_wise` enum('yes','no') NOT NULL,
  `status` enum('active','inactive','deleted') NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `plans`
--

INSERT INTO `plans` (`id`, `plan_title`, `actual_price`, `discount_price`, `exam_id`, `exam_mode`, `schedule_pdf`, `subject_wise`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Group 1', 1500, 1350, 1, 'Schedule', 'uploads/plans/18148cb16fdf4d575ea856509289ea18.pdf', 'yes', 'active', '1612785243', '1612790249');

-- --------------------------------------------------------

--
-- Table structure for table `plan_wise_points`
--

CREATE TABLE `plan_wise_points` (
  `id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `description` varchar(500) NOT NULL,
  `created_at` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `plan_wise_points`
--

INSERT INTO `plan_wise_points` (`id`, `plan_id`, `description`, `created_at`) VALUES
(16, 1, 'Testing 1', '1612790249'),
(17, 1, 'Testing 2', '1612790249');

-- --------------------------------------------------------

--
-- Table structure for table `plan_wise_subjects`
--

CREATE TABLE `plan_wise_subjects` (
  `id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `created_at` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `plan_wise_subjects`
--

INSERT INTO `plan_wise_subjects` (`id`, `plan_id`, `subject_id`, `created_at`) VALUES
(16, 1, 4, '1612790249'),
(17, 1, 1, '1612790249');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `logo` varchar(250) NOT NULL,
  `favicon` varchar(250) NOT NULL,
  `facebook` varchar(500) DEFAULT NULL,
  `youtube` varchar(500) DEFAULT NULL,
  `twitter` varchar(500) DEFAULT NULL,
  `linked_in` varchar(500) DEFAULT NULL,
  `support_mail` varchar(250) DEFAULT NULL,
  `support_number` varchar(20) DEFAULT NULL,
  `terms` text CHARACTER SET utf8 DEFAULT NULL,
  `about_us` text CHARACTER SET utf8 DEFAULT NULL,
  `privacy_policy` text CHARACTER SET utf8 DEFAULT NULL,
  `refund` text NOT NULL,
  `returns` text NOT NULL,
  `sms_sender_id` varchar(50) DEFAULT NULL,
  `sms_username` varchar(50) DEFAULT NULL,
  `sms_password` varchar(50) DEFAULT NULL,
  `created_at` varchar(25) DEFAULT NULL,
  `alternate_number` varchar(20) NOT NULL,
  `alternate_mail` varchar(150) NOT NULL,
  `updated_at` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `title`, `logo`, `favicon`, `facebook`, `youtube`, `twitter`, `linked_in`, `support_mail`, `support_number`, `terms`, `about_us`, `privacy_policy`, `refund`, `returns`, `sms_sender_id`, `sms_username`, `sms_password`, `created_at`, `alternate_number`, `alternate_mail`, `updated_at`) VALUES
(1, 'CA Exam Series', 'caexamseries.png', 'caexamseries_favicon.png', 'https://www.facebook.com/', 'https://dribbble.com/', 'https://www.twitter.com', 'https://www.instagram.com/', 'info@caexamseries.co.in', '9603960346', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '<p>Refund</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', '<p>Returns</p>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 'INFOSM', 'caexamseries', 'vizag@123', NULL, '9603960346', 'support@caexamseries.co.in', '1612524205');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `designation_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(250) NOT NULL,
  `email` varchar(150) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `password` varchar(50) NOT NULL,
  `salt` varchar(10) NOT NULL,
  `image` varchar(250) NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1=active, 0=inactive.',
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff_logs`
--

CREATE TABLE `staff_logs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `staff_id` bigint(20) UNSIGNED NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `static_content`
--

CREATE TABLE `static_content` (
  `id` int(11) NOT NULL,
  `benefits_we_offer_image` varchar(250) NOT NULL,
  `benefits_we_offer_content` varchar(1000) NOT NULL,
  `student_statistics_opted_till_date` varchar(10) NOT NULL,
  `student_statistics_qualified_students` varchar(10) NOT NULL,
  `student_statistics_5_star_rated` varchar(10) NOT NULL,
  `student_statistics_excemptions_scored` varchar(20) NOT NULL,
  `why_ca_exam_series_content` varchar(1000) NOT NULL,
  `why_ca_exam_series_embaded_link` varchar(20) NOT NULL,
  `updated_at` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `static_content`
--

INSERT INTO `static_content` (`id`, `benefits_we_offer_image`, `benefits_we_offer_content`, `student_statistics_opted_till_date`, `student_statistics_qualified_students`, `student_statistics_5_star_rated`, `student_statistics_excemptions_scored`, `why_ca_exam_series_content`, `why_ca_exam_series_embaded_link`, `updated_at`) VALUES
(1, 'uploads/6e8eaa8cac1cc8dbc572d4d0916fa852.svg', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Perferendis doloribus ratione amet.', '19,172', '13,093', '10,400', '2,347', '<h3>CA Exam Series Is a Unique &amp; Excelled Platform made for ca students to Clear their Exams.</h3>\r\n\r\n<ul>\r\n	<li>Systematic Exam Schedules</li>\r\n	<li>Balanced Question Papers</li>\r\n	<li>Legitimate Evaluation</li>\r\n	<li>Expertise Suggested Answers</li>\r\n</ul>\r\n\r\n<p>Which Gives Tremendous Practice Effective Time Management, Superlative Presentation Skills, Upsurge Confidence Levels.</p>\r\n', 'Ux-rq5yk1qU', '1612871095');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `amount` varchar(10) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Accounts', '100', 'active', '1611058760', '1611060490'),
(2, 'Law', '500', 'active', '1611060519', '1611060563'),
(3, 'Costing & FM', '500', 'active', '1612779908', NULL),
(4, 'Taxation', '450', 'active', '1612779925', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sub_menu`
--

CREATE TABLE `sub_menu` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `url` varchar(500) NOT NULL,
  `unique_name` varchar(150) NOT NULL,
  `main_menu_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` varchar(25) NOT NULL,
  `updated_at` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `sub_menu`
--

INSERT INTO `sub_menu` (`id`, `title`, `url`, `unique_name`, `main_menu_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Settings', 'settings', 'settings', 2, 1, '1610532869', NULL),
(2, 'Banners', 'banners', 'banners', 2, 1, '1610532869', NULL),
(3, 'Testimonials', 'testimonial', 'testimonial', 2, 1, '1610532869', NULL),
(4, 'Feedback', 'feedback', 'feedback', 2, 1, '1610532869', NULL),
(5, 'How It Works', 'how_it_works', 'how_it_works', 2, 1, '1610532869', NULL),
(6, 'Home Page Content', 'home_page_content', 'home_page_content', 2, 1, '1610532869', NULL),
(7, 'Subjects', 'subject', 'subject', 5, 1, '1610532869', NULL),
(8, 'Courses', 'course', 'course', 5, 1, '1610532869', NULL),
(9, 'Call Back Request', 'call_back_request', 'call_back_request', 6, 1, '1610532869', NULL),
(10, 'Feedback', 'feedback_from_contact_page', 'feedback_from_contact_page', 6, 1, '1610532869', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `testimonial`
--

CREATE TABLE `testimonial` (
  `id` int(11) NOT NULL,
  `image` varchar(250) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `name` varchar(150) NOT NULL,
  `location` varchar(250) NOT NULL,
  `status` enum('active','inactive') NOT NULL,
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `testimonial`
--

INSERT INTO `testimonial` (`id`, `image`, `description`, `name`, `location`, `status`, `created_at`, `updated_at`) VALUES
(1, 'uploads/testimonial/caexamseries_favicon.png', 'I Had Chosen Correct Platform for Practice , Enormous Number of Exams conducted by CA Exam Series helped me to know my mistakes and to present much better In ICAI Exams.I am indebted to CA Exam Series.', 'Surabhi Jain', 'Hyderabad – Telangana', 'active', '1611034023', '1611034853');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(250) NOT NULL,
  `phone_number` varchar(10) NOT NULL,
  `icia_rege_no` varchar(50) DEFAULT NULL,
  `course` int(11) DEFAULT NULL,
  `password` varchar(250) NOT NULL,
  `otp` int(4) DEFAULT NULL,
  `otp_created_at` varchar(20) DEFAULT NULL,
  `otp_verified` enum('yes','no') NOT NULL DEFAULT 'no',
  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
  `created_at` varchar(20) NOT NULL,
  `updated_at` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone_number`, `icia_rege_no`, `course`, `password`, `otp`, `otp_created_at`, `otp_verified`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Raja Ramesh', 'raja@thecolourmoon.com', '8895568688', '123456', 1, 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 'yes', 'active', '1612242750', '1612243592');

-- --------------------------------------------------------

--
-- Table structure for table `version_control`
--

CREATE TABLE `version_control` (
  `id` int(11) NOT NULL,
  `version` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `version_control`
--

INSERT INTO `version_control` (`id`, `version`) VALUES
(1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access_privilege`
--
ALTER TABLE `access_privilege`
  ADD PRIMARY KEY (`id`),
  ADD KEY `privilege_id` (`privilege_id`),
  ADD KEY `designation` (`designation`);

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username_2` (`username`),
  ADD KEY `username` (`username`);

--
-- Indexes for table `admin_logs`
--
ALTER TABLE `admin_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `admin_id` (`admin_id`),
  ADD KEY `ip_address` (`ip_address`);

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `call_back_request`
--
ALTER TABLE `call_back_request`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coursewise_subjects`
--
ALTER TABLE `coursewise_subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exams`
--
ALTER TABLE `exams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faqs`
--
ALTER TABLE `faqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_from_contact_page`
--
ALTER TABLE `feedback_from_contact_page`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `how_it_works`
--
ALTER TABLE `how_it_works`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_menu`
--
ALTER TABLE `main_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`),
  ADD KEY `exam_id` (`exam_id`);

--
-- Indexes for table `plan_wise_points`
--
ALTER TABLE `plan_wise_points`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_id` (`plan_id`);

--
-- Indexes for table `plan_wise_subjects`
--
ALTER TABLE `plan_wise_subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plan_id` (`plan_id`),
  ADD KEY `subject_id` (`subject_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `phone_number` (`phone_number`),
  ADD KEY `designation_id` (`designation_id`);

--
-- Indexes for table `staff_logs`
--
ALTER TABLE `staff_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ip_address` (`ip_address`),
  ADD KEY `staff_id` (`staff_id`);

--
-- Indexes for table `static_content`
--
ALTER TABLE `static_content`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `main_menu_id` (`main_menu_id`);

--
-- Indexes for table `testimonial`
--
ALTER TABLE `testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email` (`email`),
  ADD KEY `phone_number` (`phone_number`);

--
-- Indexes for table `version_control`
--
ALTER TABLE `version_control`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access_privilege`
--
ALTER TABLE `access_privilege`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1131;

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_logs`
--
ALTER TABLE `admin_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `call_back_request`
--
ALTER TABLE `call_back_request`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `coursewise_subjects`
--
ALTER TABLE `coursewise_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `exams`
--
ALTER TABLE `exams`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `faqs`
--
ALTER TABLE `faqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `feedback_from_contact_page`
--
ALTER TABLE `feedback_from_contact_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `how_it_works`
--
ALTER TABLE `how_it_works`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `main_menu`
--
ALTER TABLE `main_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `plan_wise_points`
--
ALTER TABLE `plan_wise_points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `plan_wise_subjects`
--
ALTER TABLE `plan_wise_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `staff_logs`
--
ALTER TABLE `staff_logs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `static_content`
--
ALTER TABLE `static_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sub_menu`
--
ALTER TABLE `sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `testimonial`
--
ALTER TABLE `testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `version_control`
--
ALTER TABLE `version_control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_logs`
--
ALTER TABLE `admin_logs`
  ADD CONSTRAINT `admin_logs_ibfk_1` FOREIGN KEY (`admin_id`) REFERENCES `admin` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `plans`
--
ALTER TABLE `plans`
  ADD CONSTRAINT `plans_ibfk_1` FOREIGN KEY (`exam_id`) REFERENCES `exams` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `plan_wise_points`
--
ALTER TABLE `plan_wise_points`
  ADD CONSTRAINT `plan_wise_points_ibfk_1` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `plan_wise_subjects`
--
ALTER TABLE `plan_wise_subjects`
  ADD CONSTRAINT `plan_wise_subjects_ibfk_1` FOREIGN KEY (`plan_id`) REFERENCES `plans` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `plan_wise_subjects_ibfk_2` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `staff_logs`
--
ALTER TABLE `staff_logs`
  ADD CONSTRAINT `staff_logs_ibfk_1` FOREIGN KEY (`staff_id`) REFERENCES `staff` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `sub_menu`
--
ALTER TABLE `sub_menu`
  ADD CONSTRAINT `sub_menu_ibfk_1` FOREIGN KEY (`main_menu_id`) REFERENCES `main_menu` (`id`) ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
