<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

    function subscribe() {
        $this->db->set('mail_id', $this->input->post('mail_id'));
        $this->db->set('created_at', time());
        $this->db->insert('subscribe_mail');
        if ($this->db->insert_id()) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

}
