<!DOCTYPE html>
<html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <base href="<?php echo base_url(); ?>">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <meta name="author" content="">
        <title><?php echo $site_settings->title; ?></title>
        <!-- Favicons-->
        <link rel="shortcut icon" href="<?php echo base_url(); ?>/uploads/<?php echo $site_settings->favicon; ?>" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap" rel="stylesheet">
        <link href="//cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/site_assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/site_assets/css/customnew.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/site_assets/css/newresponsive.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/site_assets/css/vendors.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/site_assets/css/icon_fonts/css/all_icons.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/site_assets/css/all.min.css" rel="stylesheet">
        <!-- SPECIFIC CSS -->
        <link href="<?php echo base_url(); ?>/site_assets/layerslider/css/layerslider.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>/site_assets/css/side-slider.css">

        <!-- YOUR CUSTOM CSS -->
        <link href="<?php echo base_url(); ?>/site_assets/css/blog.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/site_assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/site_assets/css/rating.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>/site_assets/css/sumoselect.min.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>/site_assets/js/jquery-2.2.4.min.js"></script>
        <script src="<?php echo base_url(); ?>/site_assets/js/jquery.sumoselect.min.js"></script>
        <link rel="stylesheet" href="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
        <link href="<?= base_url() ?>admin_assets/js/plugins/toastr/toastr.min.css" rel="stylesheet">

        <script>
            $(document).ready(function () {
                $(window).load(function () {
                    $('#welcomeModal').modal('show');
                })
            })
        </script>

        <style>
            .error{
                color: red
            }
            .navbar-dark .navbar-nav .nav-link {
                color: #fff;
                padding: 5px 25px 5px 25px;
                font-size: 20px;
            }
            .navbar-dark .navbar-nav .nav-link:focus, .navbar-dark .navbar-nav .nav-link:hover {
                color: var(--thm-yellow);
            }
            @media (min-width: 992px){
                .dropdown-menu .dropdown-toggle:after{
                    border-top: .3em solid transparent;
                    border-right: 0;
                    border-bottom: .3em solid transparent;
                    border-left: .3em solid;
                }
                .dropdown-menu .dropdown-menu{
                    margin-left:0; margin-right: 0;
                }
                .dropdown-menu li{
                    position: relative;
                }
                .nav-item .submenu{
                    display: none;
                    position: absolute;
                    left:100%; top:-7px;
                }
                .nav-item .submenu-left{
                    right:100%; left:auto;
                }
                .dropdown-menu > li:hover{ background-color: #f1f1f1 }
                .dropdown-menu > li:hover > .submenu{
                    display: block;
                }
            }
        </style>

    </head>
    <body>
        <div id="preloader">
            <div id="status"></div>
        </div>
        <div id="page">
            <div class="container-fluid pl-0 pr-0">
                <div class="row justify-content-between bg-yellow no-gutters">
                    <div class="col-lg-3 col-md-4 col-sm-12">
                        <div id="logo">
                            <a href="<?php echo base_url(); ?>home"><image src="<?php echo base_url() . 'uploads/' . $site_settings->logo; ?>" alt="<?php echo $site_settings->title; ?>"></a>
                            </span></span></a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-7 col-sm-12 align-self-center">
                        <div class="row topright-details justify-content-md-center">
                            <div class="col-lg-4 col-md-3 col-sm-4 d-lg-block d-md-block d-sm-block d-none">
                                <a href="tel:<?php echo $site_settings->support_number; ?>">
                                    <i class="fal fa-phone"></i> <span>
                                        <?php echo $site_settings->support_number; ?>
                                    </span>
                                </a>
                            </div>
                            <div class="col-lg-5 col-md-4 col-sm-4 col-6">
                                <?php if (!$this->session->userdata('web_user_id')) { ?>
                                    <a href="<?= base_url(); ?>login"><i class="fal fa-user"></i> <span>Sign In / Sign Up</span></a>
                                <?php } ?>
                                <?php if ($this->session->userdata('web_user_id')) { ?>
                                    <a href="<?= base_url(); ?>dashboard"><i class="fal fa-user"></i><span> Dashboard</span></a>
                                    <a href="<?= base_url(); ?>login/logout"><i class="fal fa-sign-out"></i><span> Logout</span></a>
                                <?php } ?>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-6">
                                <div style="cursor: pointer; position: relative;"><a href="http://caexamseries.co.in/cart" style="color:#fff;">
                                        <div class="badge badge-notify my-cart-badge">
                                            <?php
//                                        if ($this->session->userdata('cart')) {
//                                            echo count($this->session->userdata('cart'));
//                                        } else {
                                            echo '0';
//                                        }
                                            ?>
                                        </div>
                                        <i class="fal fa-shopping-cart"></i> <span>CART</span>
                                    </a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="bg-black">
                    <div class="col-lg-12 col-md-12">
                        <div class="mpbmenu">
                            <a href="#menu" class="btn_mobile">
                                <div class="hamburger hamburger--spin" id="hamburger">
                                    <div class="hamburger-box">
                                        <div class="hamburger-inner"></div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <nav class="navbar navbar-expand-lg navbar-dark d-none d-sm-block">

                            <div class="collapse navbar-collapse" id="main_nav">

                                <ul class="navbar-nav">
                                    <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>home">Home </a> </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">  Our Exams  </a>
                                        <ul class="dropdown-menu">
                                            <?php foreach ($courses_list as $list) { ?>
                                                <li><a class="dropdown-item" href="<?= base_url('courses/view/' . $list->id); ?>"><?= $list->course; ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>pass_guarantee"> Pass Guarantee </a> </li>
                                    <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>trail_exam_course"> Trail Exam </a> </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">  E - Brochure </a>
                                        <ul class="dropdown-menu">
                                            <?php foreach ($courses_list as $list) { ?>
                                                <li><a class="dropdown-item" href="<?= base_url() . $list->brochure; ?>" target="_blank"><?= $list->course; ?></a></li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">  Schedules</a>
                                        <ul class="dropdown-menu">
                                            <?php foreach ($schedules as $row) { ?>
                                                <li><a class="dropdown-item" href="#"> <?= $row->course; ?> <i class="fas fa-angle-right" style="position: absolute;
                                                                                                               right: 10px;
                                                                                                               top: 9px;"></i></a>
                                                    <ul class="submenu dropdown-menu">
                                                        <?php foreach ($row->exams as $exams) { ?>
                                                            <li><a class="dropdown-item" href="#" target="_blank"><?= $exams->title; ?> <i class="fas fa-angle-right" style="position: absolute;
                                                                                                                                           right: 10px;
                                                                                                                                           top: 9px;"></i></a>
                                                                <ul class="submenu dropdown-menu">
                                                                    <?php foreach ($exams->pdfs as $pdfs) { ?>
                                                                        <li><a class="dropdown-item" href="<?= base_url($pdfs->schedule_pdf); ?>" target="_blank"><?= $pdfs->exam_mode; ?></a></li>
                                                                    <?php } ?>
                                                                </ul>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>

                                    <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>blog"> Blog </a> </li>
                                    <li class="nav-item"> <a class="nav-link" href="<?php echo base_url(); ?>contact"> Contact Us </a> </li>
                                    <li class="nav-item"> <a class="nav-link" href="view/register"> Free MCQ </a> </li>


                                </ul>

                            </div>
                        </nav>
                        <nav id="menu" class="main-menu d-lg-none d-md-none">
                            <ul>
                                <li><span><a href="<?php echo base_url(); ?>index">Home</a></span></li>
                                <li><span><a href="#">Our Exams</a></span>
                                    <ul>
                                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>ca_foundation"> CA Foundation</a></li>
                                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>ca_inter"> CA Inter </a></li>
                                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>ca_final"> CA Final </a></li>
                                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>ipcc_old"> CA IPCC (Old)</a></li>
                                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>ipcc_final"> CA Final (Old)</a></li>
                                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>mock_exams"> Mock Exams</a></li>
                                        <li><a class="dropdown-item" href="<?php echo base_url(); ?>pass_guarantee"> Pass Guarantee</a></li>
                                    </ul>
                                </li>
                                <li><span><a href="<?php echo base_url(); ?>pass_guarantee">Pass Guarantee</a></span></li>
                                <li><span><a href="<?php echo base_url(); ?>trail_exam_course">Trail Exam</a></span></li>
                                <li><span><a href="#">E - Brochure</a></span>
                                    <ul>
                                        <li><a class="dropdown-item" href="#" target="_blank">CPT</a></li>
                                        <li><a class="dropdown-item" href="site_assets/images/01 CA IPCC OLD - NOV 2020.pdf" target="_blank">IPCC(Old)</a></li>
                                        <li><a class="dropdown-item" href="site_assets/images/03 CA FINAL OLD - NOV 2020.pdf"  target="_blank">FINAL</a></li>
                                        <li><a class="dropdown-item" href="#" target="_blank">CPT(New)</a></li>
                                        <li><a class="dropdown-item" href="site_assets/images/02 CA INTER - NOV 2020.pdf" target="_blank">IPCC(New)</a></li>
                                        <li><a class="dropdown-item" href="site_assets/images/04 CA FINAL NEW - NOV 2020.pdf" target="_blank">FINAL(New)</a></li>
                                        <li><a class="dropdown-item" href="site_assets/images/01 CA IPCC - PG - NOV 2020.pdf" target="_blank">Pass Guarantee (IPCC)</a></li>
                                        <li><a class="dropdown-item" href="site_assets/images/02 CA INTER - PG - NOV 2020.pdf" target="_blank">Pass Guarantee (INTER)</a></li>
                                    </ul>
                                </li>
                                <li><span><a href="#">Schedules</a></span>
                                    <ul>
                                        <li><a class="dropdown-item" href="#"> Pass Guarantee</a>
                                            <ul>
                                                <li><a class="dropdown-item" href="site_assets/images/IPCC%20Old%20PG%20-%20May%202020.pdf" target="_blank">CA IPCC (Old)</a></li>
                                                <li><a class="dropdown-item" href="site_assets/images/IPCC%20NEW%20PG%20-%20May%202020.pdf" target="_blank">CA INTER (New)</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item" href="#"> CA Foundation</a>
                                            <ul>
                                                <li><a class="dropdown-item" href="#">Chapter Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Seciton Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Unit Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item" href="#"> CA Inter</a>
                                            <ul>
                                                <li><a class="dropdown-item" href="#">Chapter Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Seciton Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Unit Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item" href="#"> CA Final</a>
                                            <ul>
                                                <li><a class="dropdown-item" href="#">Chapter Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Seciton Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Unit Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item" href="#"> CA IPCC (Old)</a>
                                            <ul>
                                                <li><a class="dropdown-item" href="#">Chapter Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Seciton Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Unit Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item" href="#"> CA Final (Old)</a>
                                            <ul>
                                                <li><a class="dropdown-item" href="#">Chapter Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Seciton Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Unit Wise</a>
                                                    <ul>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Schedule</a></li>
                                                        <li><a class="dropdown-item" href="#" target="_blank">Un Schedule</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><span><a href="<?php echo base_url(); ?>blog">Blog</a></span></li>
                                <li><span><a href="<?php echo base_url(); ?>contact">Contact Us</a></span></li>
                                <li><span><a href="view/register">Free MCQ</a></span></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!-- /header -->