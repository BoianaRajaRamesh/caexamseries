<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

    private $model = "Users_model";

    function __construct() {
        parent::__construct();
        $this->load->model("admin/" . $this->model);
    }

    public function index() {
        $this->data['page'] = 'Login';
        $this->web_view('login', $this->data);
    }

    public function register() {
        $this->data['page'] = 'Register';
        $this->web_view('register', $this->data);
    }

    function register_user() {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("name", "name", "required", array(
                'required' => 'name cannot be empty',
            ));
            $this->form_validation->set_rules("email", "email", "required|is_unique[users.email]", array(
                'required' => 'email cannot be empty',
                "is_unique" => "email must be unique.",
            ));
            $this->form_validation->set_rules("phone_number", "phone_number|is_unique[users.phone_number]", "required", array(
                'required' => 'phone number cannot be empty',
                "is_unique" => "phone number must be unique.",
            ));
            $this->form_validation->set_rules("password", "password", "required", array(
                'required' => 'password cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {
                $this->data['page'] = 'Register';
                $this->web_view('register', $this->data);
            } else {
                $otp = rand(1000, 9999);
                $data = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'phone_number' => $this->input->post('phone_number'),
                    'course' => $this->input->post('course'),
                    'password' => md5($this->input->post('password')),
                    'icia_rege_no' => $this->input->post('icia_rege_no'),
                    'created_at' => time(),
                    'status' => 'active',
                    'otp' => $otp,
                    'otp_created_at' => time(),
                );
                $res = $this->{$this->model}->add($data);
                if ($res) {
                    $msg = "Hello " . $this->input->post('name') . ", Thank Q for registration in CA ExamSeries. Your OTP is:" . $otp . " and valid for 10 minutes";
                    send_sms($this->input->post('phone_number'), $msg, '123456');
                    $this->send_mail("CA ExamSeries OTP", $msg, $this->input->post('email'));
                    $data = array(
                        'web_user_id' => $res,
                        'web_user_name' => $this->input->post('name'),
                        'web_mobile_number' => $this->input->post('phone_number'),
                    );
                    $this->session->set_userdata($data);
                    redirect(base_url() . 'login/otp');
                    $this->session->set_flashdata('success_message', '"Success!","Login Successfully!"');
                    redirect(base_url() . 'dashboard');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'login/register');
                }
            }
        }
    }

    function login_user() {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("username", "username", "required", array(
                'required' => 'username cannot be empty',
            ));
            $this->form_validation->set_rules("password", "password", "required", array(
                'required' => 'password cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {
                $this->data['page'] = 'Login';
                $this->web_view('login', $this->data);
            } else {
                $user_details = $res = $this->{$this->model}->get_details_by_mobile_or_email($this->input->post('username'));
                if ($res) {
                    if ($res->password == md5($this->input->post('password'))) {
                        $data = array(
                            'web_user_id' => $res->id,
                            'web_user_name' => $res->name,
                            'web_mobile_number' => $res->phone_number,
                        );
                        $this->session->set_userdata($data);
                        if ($res->otp_verified == 'yes') {
                            $this->session->set_flashdata('success_message', '"Success!","Login Successfully!"');
                            redirect(base_url() . 'dashboard');
                        } else {
                            $otp = rand(1000, 9999);
                            $data = array(
                                'otp' => $otp,
                                'otp_created_at' => time(),
                                'updated_at' => time(),
                            );
                            $result = $this->{$this->model}->update($user_details->id, $data);

                            $msg = "Hello " . $user_details->name . ", Your OTP is:" . $otp . " and valid for 10 minutes";
                            send_sms($user_details->phone_number, $msg, '123456');
                            $this->send_mail("CA ExamSeries OTP", $msg, $user_details->email);

                            $this->session->set_flashdata('success_message', '"Success!","OTP Sent Successfully!"');
                            redirect(base_url() . 'login/otp');
                        }
                    } else {
                        $this->session->set_flashdata('error_message', '"Error Occured!","Password not matched"');
                        redirect(base_url() . 'login');
                    }
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Username not found"');
                    redirect(base_url() . 'login');
                }
            }
        }
    }

    function logout() {
        $this->session->unset_userdata('web_user_id');
        $this->session->unset_userdata('web_user_name');
        $this->session->set_flashdata('success_message', '"Success!","Logout Successfully!"');
        redirect(base_url() . 'login');
    }

    function otp() {
        $this->data['page'] = 'OTP';
        $this->web_view('otp', $this->data);
    }

    function otp_verify() {
        $this->form_validation->set_rules("otp", "OTP", "required|min_length[4]|max_length[4]|is_natural", array(
            'required' => 'OTP cannot be empty',
            "is_natural" => "Please enter valid OTP",
            "min_length" => "Enter 4 digit OTP",
            "max_length" => "Enter 4 digit OTP",
        ));
        if ($this->form_validation->run() == FALSE) {
            $this->data['page'] = 'OTP';
            $this->web_view('otp', $this->data);
        } else {
            $user_id = $this->session->userdata('web_user_id');
            $user_details = $result = $this->{$this->model}->get_details($user_id);

            if ($result) {
                if ($result->otp != $this->input->post('otp')) {
                    $this->session->set_flashdata('error_message', '"Error Occured!","You entered OTP is wrong! Please try again."');
                    redirect(base_url() . 'login/otp');
                } else {
                    $otp_time = $result->otp_created_at;
                    $current_time = time();
                    $sub = ($current_time - $otp_time) / 60;
                    if ($sub <= 10) {
                        $data = array(
                            'otp' => NULL,
                            'otp_created_at' => NULL,
                            'otp_verified' => 'yes',
                            'updated_at' => time(),
                        );
                        $result = $this->{$this->model}->update($user_id, $data);
                        if ($result) {
                            $this->session->set_flashdata('success_message', '"Success!","OTP Verified Successfully!"');
                            redirect(base_url() . 'dashboard');
                        } else {
                            $this->session->set_flashdata('error_message', '"Error Occured!","Something went wrong! Please try again."');
                            redirect(base_url() . 'login/otp');
                        }
                    } else {
                        $this->session->set_flashdata('error_message', '"Error Occured!","OTP expired"');
                        redirect(base_url() . 'login/otp');
                    }
                }
            } else {
                $this->session->set_flashdata('error_message', '"Error Occured!","Details Not Found"');
                redirect(base_url() . 'login/otp');
            }
        }
    }

    function resend() {
        $user_id = $this->session->userdata('web_user_id');
        $user_details = $this->{$this->model}->get_details($user_id);
        $data = array(
            'otp' => $otp,
            'otp_created_time' => time(),
            'updated_at' => time(),
        );
        $result = $this->{$this->model}->update($user_id, $data);
        if ($result) {
            $msg = "Hello " . $user_details->name . ", Your OTP is:" . $otp . " and valid for 10 minutes";
            send_sms($user_details->phone_number, $msg, '123456');

            $this->session->set_flashdata('success_message', '"Success!","OTP Resent Successfully!"');
            redirect(base_url() . 'login/otp');
        } else {
            $this->session->set_flashdata('error_message', '"Error Occured!","Something went wrong! Please try again."');
            redirect(base_url() . 'login/otp');
        }
    }

    function change_number() {
        $this->data['page'] = 'Change Number';
        $this->web_view('change_number', $this->data);
    }

    function update_mobile_number() {
        $this->form_validation->set_rules("mobile_number", "phone number", "required|min_length[10]|max_length[10]|is_natural|is_unique[users.phone_number]", array(
            'required' => 'phone number cannot be empty',
            "is_natural" => "Please enter valid phone number",
            "min_length" => "Enter 10 digit phone number",
            "max_length" => "Enter 10 digit phone number",
            "is_unique" => "phone number must be unique.",
        ));
        if ($this->form_validation->run() == FALSE) {
            $this->data['page'] = 'Change Number';
            $this->web_view('change_number', $this->data);
        } else {
            $user_id = $this->session->userdata('web_user_id');
            $user_details = $this->{$this->model}->get_details($user_id);
            $otp = rand(1000, 9999);
            $data = array(
                'phone_number' => $this->input->post('mobile_number'),
                'otp' => $otp,
                'otp_created_at' => time(),
                'updated_at' => time(),
            );
            $result = $this->{$this->model}->update($user_id, $data);
            if ($result) {
                $msg = "Hello " . $user_details->name . ", Your OTP is:" . $otp . " and valid for 10 minutes";
                send_sms($user_details->phone_number, $msg, '123456');
                $this->send_mail("CA ExamSeries OTP", $msg, $user_details->email);

                $this->session->set_flashdata('success_message', '"Success!","OTP Sent Successfully!"');
                redirect(base_url() . 'login/otp');
            } else {
                $this->session->set_flashdata('error_message', '"Error Occured!","Something went wrong! Please try again."');
                redirect(base_url() . 'login/change_number');
            }
        }
    }

    function forgot_password() {
        $this->data['page'] = 'Forgot Password';
        $this->web_view('forgot_password', $this->data);
    }

    function update_password() {
        if ($this->input->post('submit')) {
            $details = $this->db->where('email', $this->input->post('email'))->get('users');
            if ($details->num_rows() == 1) {
                $password = randomPassword(8);
                $this->db->set('password', md5($password));
                $this->db->set('updated_at', time());
                $this->db->where('id', $details->id);
                $this->db->update('users');
                $message = "Dear $details->name, Your new password is: $password";
                $this->send_mail("Forgot password", $message, $details->email);

                $this->session->set_flashdata('success_message', '"Success!","New Password Sent To Mail Successfully!"');
                redirect(base_url() . 'login');
            } else {
                $this->session->set_flashdata('error_message', '"Error Occured!","Something went wrong! Please try again."');
                redirect(base_url() . 'login/forgot_password');
            }
        }
    }

}
