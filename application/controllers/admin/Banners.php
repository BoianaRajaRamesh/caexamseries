<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/banners_model');
        $this->data['unique_name'] = 'masters';
        $this->data['sub_unique_name'] = 'banners';
        $this->data['page_title'] = 'Banners';
    }

    function index() {
        $this->data['banners'] = $this->banners_model->get_banners();
        $this->data['form_title'] = 'Add';
        if ($this->input->post('submit')) {
            $data = array();

            $config['upload_path'] = 'uploads/';
            $config['allowed_types'] = 'jpg|jpeg|JPG|JPEG|png|PNG';
            $config['overwrite'] = False;
            //Image upload script starts here
            $config['file_name'] = time() . '.' . pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image')) {
                $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                $file_name5 = 'uploads/' . $upload_data['file_name'];
                $data['image'] = $file_name5;
            }


            $config['upload_path'] = 'uploads/';
            $config['allowed_types'] = 'jpg|jpeg|JPG|JPEG|png|PNG';
            $config['overwrite'] = False;
            //Image upload script starts here
            $config['file_name'] = time() . '.' . pathinfo($_FILES['mobile_slider']['name'], PATHINFO_EXTENSION);
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('mobile_slider')) {
                $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                $file_name = 'uploads/' . $upload_data['file_name'];
                $data['mobile_slider'] = $file_name;
            }

            $data['status'] = $this->input->get_post('status');
            $data['priority'] = $this->input->get_post('priority');
            $result = $this->banners_model->add($data);
            if (isset($result)) {

                $this->session->set_flashdata('success_message', '"Success!","Added Successfully!"');
                redirect(base_url() . 'admin/banners');
            } else {
                $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                redirect(base_url() . 'admin/banners');
            }
        }
        $this->admin_view('banners');
    }

    function update($id) {
        $this->data['form_title'] = 'Update';
        if ((isset($id)) && ($id != '')) {
            $parameters = array();
            $parameters['id'] = $id;
            $this->data['banners_details'] = $this->banners_model->get_banners_details($parameters);

            if ($this->input->post('submit')) {
                $data = array();
                if ($_FILES['image']['name'] != '') {
                    $config['upload_path'] = 'uploads/';
                    $config['allowed_types'] = 'jpg|jpeg|JPG|JPEG|png|PNG';
                    $config['overwrite'] = False;
                    //Image upload script starts here
                    $config['file_name'] = time() . '.' . pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('image')) {
                        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                        $file_name5 = 'uploads/' . $upload_data['file_name'];
                        $data['image'] = $file_name5;
                    }
                }
                if ($_FILES['mobile_slider']['name'] != '') {
                    $config['upload_path'] = 'uploads/';
                    $config['allowed_types'] = 'jpg|jpeg|JPG|JPEG|png|PNG';
                    $config['overwrite'] = False;
                    //Image upload script starts here
                    $config['file_name'] = time() . '.' . pathinfo($_FILES['mobile_slider']['name'], PATHINFO_EXTENSION);
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('mobile_slider')) {
                        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                        $file_name = 'uploads/' . $upload_data['file_name'];
                        $data['mobile_slider'] = $file_name;
                    }
                }
                $data['status'] = $this->input->get_post('status');
                $data['priority'] = $this->input->get_post('priority');
                if ($this->banners_model->update($parameters, $data)) {
                    $this->session->set_flashdata('success_message', '"Success!","Updated Successfully!"');
                    redirect(base_url() . 'admin/banners');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'admin/banners/update/' . $parameters['id']);
                }
            }
            $this->data['page_title'] = 'Update Slider';
            $this->data['banners'] = $this->banners_model->get_banners();
            $this->admin_view('banners');
        } else {
            redirect(base_url() . 'admin/banners');
        }
    }

    function delete($id) {
        if ((isset($id)) && ($id != '')) {
            $parameters = array();
            $parameters['id'] = $id;

            if ($this->banners_model->delete($parameters)) {

                $this->session->set_flashdata('success_message', "'Deleted Successfully', 'Success'");
                redirect(base_url() . 'admin/banners');
            } else {
                $this->session->set_flashdata('error_message', "'Please Try Again', 'Error'");
                redirect(base_url() . 'admin/banners');
            }
        } else {
            redirect(base_url() . 'admin/banners');
        }
    }

}
