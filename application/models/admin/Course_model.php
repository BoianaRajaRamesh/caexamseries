<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Course_model extends CI_Model {

    private $table = "courses";

    function get_data() {
        $this->db->order_by("id", "desc");
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
    }

    function get_data_for_banners() {
        $this->db->order_by("id", "asc");
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
    }

    function get_active_data() {
        $this->db->order_by("id", "asc");
        $this->db->where('status', 'active');
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
    }

    function get_subjects() {
        $this->db->order_by("id", "desc");
        $data = $this->db->get('subjects');
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
    }

    function get_details($id) {
        $this->db->where('id', $id);
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->row();
            return $result;
        }
    }

    function get_banner_details($id) {
        $this->db->where('id', $id);
        $data = $this->db->get('courses_banners');
        if ($data->num_rows() > 0) {
            $result = $data->row();
            return $result;
        }
    }

    function add($data) {
        $this->db->set($data);
        $this->db->insert($this->table);
        if ($this->db->insert_id()) {
            return $this->db->insert_id();
        }
    }

    function add_banner($data) {
        $this->db->set($data);
        $this->db->insert('courses_banners');
        if ($this->db->insert_id()) {
            return $this->db->insert_id();
        }
    }

    function update($id, $data) {
        $this->db->set($data);
        $this->db->where('id', $id);
        if ($this->db->update($this->table)) {
            return true;
        }
    }

    function update_banner($id, $data) {
        $this->db->set($data);
        $this->db->where('id', $id);
        if ($this->db->update('courses_banners')) {
            return true;
        }
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

    function delete_banner($id) {
        $this->db->where('id', $id);
        $this->db->delete('courses_banners');
        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

    function get_schedules_data() {
        $this->db->select('id, course');
        $this->db->where('status', 'active');
        $data = $this->db->get('courses')->result();
        foreach ($data as $row) {
            $row->exams = $this->db->select('id,title')->where('status', 'active')->get('exams')->result();
            foreach ($row->exams as $item) {
                $item->pdfs = $this->db->select('exam_mode, schedule_pdf')->where('course_id', $row->id)->where('exam_id', $item->id)->where('exam_mode !=', 'Other')->group_by('exam_mode')->get('plans')->result();
            }
        }
        return $data;
    }

    function get_banners() {
        $this->db->select("courses_banners.*, courses.course")->join("courses", "courses.id = courses_banners.course_id");
        return $this->db->order_by('courses_banners.id', 'desc')->get('courses_banners')->result();
    }

}
