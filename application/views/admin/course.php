<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>/dashboard">Dashboard</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $page_title; ?></h5>
                    <a class="btn btn-success btn-sm pull-right" style="margin-top: -5px" href="<?= base_url(); ?>admin/course/add"><i class="fa fa-plus"></i> Add</a>
                </div>
                <div class="ibox-content">
                    <!-- table-responsive -->
                    <div class="">
                        <table id="myTable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Courses</th>
                                    <th>Brochure</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($list) && (count($list) > 0)) {
                                    $i = 1;
                                    foreach ($list as $item) {
                                        ?>
                                        <tr>
                                            <td><?= $i ?></td>
                                            <td><?= $item->course ?></td>
                                            <td><a href="<?= base_url() . $item->brochure ?>" target="_blank"><i class="fa fa-file-pdf-o fa-2x" ></i></a></td>
                                            <th class="text-capitalize"><?= $item->status ?></th>
                                            <td>
                                                <a href="<?= base_url() ?>admin/course/update/<?= $item->id ?>" class="btn btn-sm btn-circle btn-success"><i class="fa fa-edit"></i></a>
                                                <!--<button type="button" class="btn btn-sm btn-circle btn-danger delete_item" value="<?= $item->id ?>"><i class="fa fa-trash"></i></button>-->
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('admin/includes/footer');
?>
<script type="text/javascript">
    $('#text').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9 ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $(document).ready(function () {
        $('#myTable').DataTable();
        //Delete Confirmation Script starts here
        $(document).on("click", '.delete_item', function () {
            var del_id = $(this).val();
            swal({
                title: "Are you sure?",
                text: "You want to Delete entire data related to this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                window.location = "<?= base_url() ?>admin/course/delete/" + del_id;
            });
        });
        //Delete Confirmation Script ends here
    });
</script>