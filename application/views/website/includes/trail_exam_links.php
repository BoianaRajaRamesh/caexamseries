<div class="card">
	<div class="item1-links  mb-0">
		<a href="trail_exam" class="d-flex border-bottom"><span class="icon1 mr-3"><i class="fal fa-edit"></i></span> Trail Exam</a>
		<a href="myresult_trail" class="d-flex border-bottom"><span class="icon1 mr-3"><i class="fal fa-check"></i></span> My Results</a>
		<a href="preformance_trail" class="d-flex border-bottom"><span class="icon1 mr-3"><i class="fal fa-chart-bar"></i></span> Performance Report</a>
		<a href="upload_history_trail" class="d-flex border-bottom"><span class="icon1 mr-3"><i class="fal fa-upload"></i></span> Upload History </a>
		<a href="notifications_trail" class="d-flex border-bottom"><span class="icon1 mr-3"><i class="fal fa-list"></i></span> Notifications </a>
		<a href="supportbox_trail" class="d-flex border-bottom"><span class="icon1 mr-3"><i class="fal fa-box-alt"></i></span> Support Box  </a>
		<a href="notes_trail" class="d-flex border-bottom"><span class="icon1 mr-3"><i class="fal fa-book"></i></span> Notes   </a>
		<a href="videos_trail" class="d-flex border-bottom"><span class="icon1 mr-3"><i class="fal fa-video"></i></span> Videos    </a>
		<a href="guidelines_course_trial" class="d-flex border-bottom"><span class="icon1 mr-3"><i class="fal fa-align-justify"></i></span> Guidelines     </a>
		<a href="#" class="d-flex btn btn-warning text-center text-dark"><strong style="font-size: 18px"><span class="icon1 mr-3"><i class="fal fa-book"></i></span> BUY NOW</strong></a>
	</div>
</div>