<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>admin/dashboard">Dashboard</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div>
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Update <?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="" enctype="multipart/form-data" autocomplete="off">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Benefits We Offer Image</label>
                                <div class="col-sm-12">
                                    <input type="file" name="benefits_we_offer_image" class="form-control">
                                    <?php if (isset($details)) { ?>
                                        <span>Only Image formats are available <b><a target="_blank" href="<?= base_url($details->benefits_we_offer_image); ?>">Previous Image</a></b></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Benefits We Offer Content</label>
                                <div class="col-sm-12">
                                    <input type="text" name="benefits_we_offer_content" class="form-control" required="" value="<?php if (isset($details)) echo $details->benefits_we_offer_content ?>" placeholder="Benefits We Offer Content">
                                </div>
                                <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12">Student Statistics Opted Till Date</label>
                                <div class="col-sm-12">
                                    <input type="text" placeholder="Student Statistics Opted Till Date Count" name="student_statistics_opted_till_date" value="<?php if (isset($details)) echo $details->student_statistics_opted_till_date ?>" class="form-control">
                                </div>
                                <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12">Student Statistics Qualified Students</label>
                                <div class="col-sm-12">
                                    <input type="text" placeholder="Student Statistics Qualified Students Count" name="student_statistics_qualified_students" value="<?php if (isset($details)) echo $details->student_statistics_qualified_students ?>" class="form-control">
                                </div>
                                <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12">Student Statistics 5 Star Rated</label>
                                <div class="col-sm-12">
                                    <input type="text" placeholder="Student Statistics 5 Star Rated Count" name="student_statistics_5_star_rated" value="<?php if (isset($details)) echo $details->student_statistics_5_star_rated ?>" class="form-control">
                                </div>
                                <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12">Student Statistics Exemptions Scored</label>
                                <div class="col-sm-12">
                                    <input type="text" placeholder="Student Statistics Excemptions Scored Count" name="student_statistics_excemptions_scored" value="<?php if (isset($details)) echo $details->student_statistics_excemptions_scored ?>" class="form-control">
                                </div>
                                <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-sm-12">Why CA Exam Series Content</label>
                                <div class="col-sm-12">
                                    <textarea name="why_ca_exam_series_content"><?php if (isset($details)) echo $details->why_ca_exam_series_content ?></textarea>
                                    <!--<input type="text" placeholder="Why CA Exam Series Content" name="why_ca_exam_series_content" value="<?php if (isset($details)) echo $details->student_statistics_excemptions_scored ?>" class="form-control number">-->
                                </div>
                                <?php echo form_error('why_ca_exam_series_content', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12">Why CA Exam Series Embedded Link</label>
                                <div class="col-sm-12">
                                    <input type="text" placeholder="Why CA Exam Series Embedded Link" name="why_ca_exam_series_embaded_link" value="<?php if (isset($details)) echo $details->why_ca_exam_series_embaded_link ?>" class="form-control">
                                </div>
                                <?php echo form_error('why_ca_exam_series_embaded_link', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-2 col-md-offset-5">
                            <label class="col-sm-12"> &nbsp; </label>
                            <input type="submit" name="submit" class="btn btn-primary btn-block" value="Update">
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/includes/footer'); ?>
<script type="text/javascript">
    CKEDITOR.replace("why_ca_exam_series_content");
</script>