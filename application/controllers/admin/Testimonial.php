<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class Testimonial extends MY_Controller {

    private $model = "Testimonial_model";

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/' . $this->model);
        $this->data['unique_name'] = 'masters';
        $this->data['sub_unique_name'] = 'testimonial';
    }

    function index() {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("description", "description", "required", array(
                'required' => 'description cannot be empty',
            ));
            $this->form_validation->set_rules("name", "name", "required", array(
                'required' => 'name cannot be empty',
            ));
            $this->form_validation->set_rules("location", "location", "required", array(
                'required' => 'location cannot be empty',
            ));
            $this->form_validation->set_rules("status", "Status", "required", array(
                'required' => 'Status cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {

            } else {
                $path = 'uploads/testimonial/';
                $image = $this->file_upload_with_specific_path('image', $path);
                $data = array(
                    'image' => $path . $image,
                    'description' => $this->input->post('description'),
                    'name' => $this->input->post('name'),
                    'location' => $this->input->post('location'),
                    'status' => $this->input->post('status'),
                    'created_at' => time(),
                );
                $res = $this->{$this->model}->add($data);
                if ($res) {
                    $this->session->set_flashdata('success_message', '"Success!","Added Successfully!"');
                    redirect(base_url() . 'admin/testimonial');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'admin/testimonial');
                }
            }
        }
        $this->data['page_title'] = 'Testimonials';
        $this->data['form_title'] = 'Add';
        $this->data['list'] = $this->{$this->model}->get_data();
        $this->admin_view('testimonial');
    }

    function update($id) {
        if ((isset($id)) && ($id != '')) {
            $this->data['details'] = $this->{$this->model}->get_details($id);

            if ($this->input->post('submit')) {
                $this->form_validation->set_rules("description", "description", "required", array(
                    'required' => 'description cannot be empty',
                ));
                $this->form_validation->set_rules("name", "name", "required", array(
                    'required' => 'name cannot be empty',
                ));
                $this->form_validation->set_rules("location", "location", "required", array(
                    'required' => 'location cannot be empty',
                ));
                $this->form_validation->set_rules("status", "Status", "required", array(
                    'required' => 'Status cannot be empty',
                ));
                if ($this->form_validation->run() == FALSE) {

                } else {
                    if ($_FILES['image']['name'] != '') {
                        $path = 'uploads/testimonial/';
                        $image = $path . $this->file_upload_with_specific_path('image', $path);
                    } else {
                        $image = $this->data['details']->image;
                    }

                    $data = array(
                        'image' => $image,
                        'description' => $this->input->post('description'),
                        'name' => $this->input->post('name'),
                        'location' => $this->input->post('location'),
                        'status' => $this->input->post('status'),
                        'updated_at' => time(),
                    );
                    $res = $this->{$this->model}->update($id, $data);
                    if ($res) {
                        $this->session->set_flashdata('success_message', '"Success!","Updated Successfully!"');
                        redirect(base_url() . 'admin/testimonial');
                    } else {
                        $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                        redirect(base_url() . 'admin/testimonial/update/' . $id);
                    }
                }
            }
            $this->data['page_title'] = 'Testimonials';
            $this->data['form_title'] = 'Update';
            $this->data['list'] = $this->{$this->model}->get_data();
            $this->admin_view('testimonial');
        }
    }

    function delete($id) {
        if ((isset($id)) && ($id != '')) {
            if ($this->{$this->model}->delete($id)) {
                $this->session->set_flashdata('success_message', "'Deleted Successfully', 'Success'");
                redirect(base_url() . 'admin/testimonial');
            } else {
                $this->session->set_flashdata('error_message', "'Please Try Again', 'Error'");
                redirect(base_url() . 'admin/testimonial');
            }
        } else {
            redirect(base_url() . 'admin/testimonial');
        }
    }

}
