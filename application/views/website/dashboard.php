<main>
    <div class="container padd-50 px-sm-5 px-5">
        <div class="row">
            <div class="col-lg-3">
                <?php $this->load->view('website/includes/trail_exam_links'); ?>
            </div>
            <div class="col-lg-9">
                <h2>Trail Exam</h2><hr>
                <div class="row fz-18 justify-content-center">
                    <div class="col-lg-3">Exam :</div>
                    <div class="col-lg-5">IPCC/INTER Trial Exam</div>
                </div>
                <div class="row pb-4 fz-18 justify-content-center">
                    <div class="col-lg-3">Subject :</div>
                    <div class="col-lg-5">Group 1 &amp; 2</div>
                </div>
                <hr>
                <div class="row justify-content-center">
                    <div class="col-lg-3 col-md-3 mb-2"><button type="button" class="btn btn-warning btn-block" data-toggle="modal" data-target="#trialDetails"><i class="fal fa-download"></i> Download</button></div>
                    <div class="col-lg-3 col-md-3 mb-2"><button type="button" class="btn btn-warning  btn-block" data-toggle="modal" data-target="#trialUpload"><i class="fal fa-upload"></i> Upload</button></div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel-default">
                            <h2>Instructions</h2>
                            <ol class="listinst">
                                <li>Download Question Paper by Clicking Below Download Button </li>
                                <li>Write Exam on Paper  and mention Page Number on Top Right Corner.</li>
                                <li>Do Mention Your Name , Regristration Number (Log-in ID) ,Mobile Number , Email ID on Top Left Corner</li>
                                <li>Download Cam Scanner App ,  Scan the Answer Copies in an Order and make a PDf then Upload</li>
                                <li>Your file should Meet all our Guidelines(Attached in Question Paper) And Result Will declared with in 48 Hours   </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row">
                    <div class="col-lg-4 col-md-6 pb-2">
                        <div class="box">
                            <img src="http://i3.ytimg.com/vi/oIOJ3fplxqY/hqdefault.jpg" alt="" class="img-fluid">
                            <ul class="icon">
                                <li><a href="https://www.youtube.com/watch?v=oIOJ3fplxqY" data-fancybox=""><i class="fa fa-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 pb-2">
                        <div class="box">
                            <img src="http://i3.ytimg.com/vi/anCHFhlwT-Q/hqdefault.jpg" alt="" class="img-fluid">
                            <ul class="icon">
                                <li><a href="https://www.youtube.com/watch?v=anCHFhlwT-Q" data-fancybox=""><i class="fa fa-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 pb-2">
                        <div class="box">
                            <img src="http://i3.ytimg.com/vi/iEo1WWrfW8g/hqdefault.jpg" alt="" class="img-fluid">
                            <ul class="icon">
                                <li><a href="https://www.youtube.com/watch?v=iEo1WWrfW8g" data-fancybox=""><i class="fa fa-play"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="trialDetails" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Enquiry Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Enter Name"/>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Enter Mobile"/>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Enter Email Address"/>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="trialUpload" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Upload Details</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fal fa-times-circle"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Enter Mobile"/>
                        </div>
                        <div class="form-group">
                            <label>Upload File</label>
                            <input type="file" class="form-control" placeholder="Upload File"/>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-warning">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
//print_r($this->session->userdata());
$this->load->view('website/includes/footer');
?>