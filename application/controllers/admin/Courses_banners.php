<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class Courses_banners extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/Course_model');
        $this->data['unique_name'] = 'courses';
        $this->data['sub_unique_name'] = 'courses_banners';
        $this->data['page_title'] = 'Courses Banners';
    }

    function index() {
        $this->data['banners'] = $this->Course_model->get_banners();
        $this->data['courses'] = $this->Course_model->get_data_for_banners();
        $this->data['form_title'] = 'Add';
        if ($this->input->post('submit')) {
            $data = array();

            $config['upload_path'] = 'uploads/';
            $config['allowed_types'] = 'jpg|jpeg|JPG|JPEG|png|PNG';
            $config['overwrite'] = False;
            //Image upload script starts here
            $config['file_name'] = time() . '.' . pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
            $this->load->library('upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('image')) {
                $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                $file_name5 = 'uploads/' . $upload_data['file_name'];
                $data['image'] = $file_name5;
            }

            $data['status'] = $this->input->get_post('status');
            $data['priority'] = $this->input->get_post('priority');
            $data['course_id'] = $this->input->get_post('course_id');
            $data['created_at'] = time();
            $result = $this->Course_model->add_banner($data);
            if (isset($result)) {
                $this->session->set_flashdata('success_message', '"Success!","Added Successfully!"');
                redirect(base_url() . 'admin/courses_banners');
            } else {
                $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                redirect(base_url() . 'admin/courses_banners');
            }
        }
        $this->admin_view('courses_banners');
    }

    function update($id) {
        $this->data['banners'] = $this->Course_model->get_banners();
        $this->data['courses'] = $this->Course_model->get_data_for_banners();
        $this->data['form_title'] = 'Update';
        if ((isset($id)) && ($id != '')) {
            $this->data['banners_details'] = $this->Course_model->get_banner_details($id);

            if ($this->input->post('submit')) {
                $data = array();
                if ($_FILES['image']['name'] != '') {
                    $config['upload_path'] = 'uploads/';
                    $config['allowed_types'] = 'jpg|jpeg|JPG|JPEG|png|PNG';
                    $config['overwrite'] = False;
                    //Image upload script starts here
                    $config['file_name'] = time() . '.' . pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
                    $this->load->library('upload', $config);
                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('image')) {
                        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                        $file_name5 = 'uploads/' . $upload_data['file_name'];
                        $data['image'] = $file_name5;
                    }
                }

                $data['status'] = $this->input->get_post('status');
                $data['priority'] = $this->input->get_post('priority');
                $data['course_id'] = $this->input->get_post('course_id');
                $data['updated_at'] = time();
                if ($this->Course_model->update_banner($id, $data)) {
                    $this->session->set_flashdata('success_message', '"Success!","Updated Successfully!"');
                    redirect(base_url() . 'admin/courses_banners');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'admin/courses_banners/update/' . $id);
                }
            }
            $this->data['page_title'] = 'Courses Banners';
            $this->admin_view('courses_banners');
        } else {
            redirect(base_url() . 'admin/courses_banners');
        }
    }

    function delete_banner($id) {
        if ((isset($id)) && ($id != '')) {
            if ($this->Course_model->delete_banner($id)) {
                $this->session->set_flashdata('success_message', "'Deleted Successfully', 'Success'");
                redirect(base_url() . 'admin/courses_banners');
            } else {
                $this->session->set_flashdata('error_message', "'Please Try Again', 'Error'");
                redirect(base_url() . 'admin/courses_banners');
            }
        } else {
            redirect(base_url() . 'admin/courses_banners');
        }
    }

}
