<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_page_content extends MY_Controller {

    private $model = "Home_page_content_model";

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/' . $this->model);
        $this->data['unique_name'] = 'masters';
        $this->data['sub_unique_name'] = 'home_page_content';
    }

    function index() {
        $this->data['details'] = $this->{$this->model}->get_details(1);
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("benefits_we_offer_content", "benefits we offer content", "required", array(
                'required' => 'benefits we offer content cannot be empty',
            ));
            $this->form_validation->set_rules("student_statistics_opted_till_date", "student statistics opted till date", "required", array(
                'required' => 'student statistics opted till date cannot be empty',
            ));
            $this->form_validation->set_rules("student_statistics_qualified_students", "student statistics qualified students", "required", array(
                'required' => 'student statistics qualified students cannot be empty',
            ));
            $this->form_validation->set_rules("student_statistics_5_star_rated", "student statistics 5 star rated", "required", array(
                'required' => 'student statistics 5 star rated cannot be empty',
            ));
            $this->form_validation->set_rules("student_statistics_excemptions_scored", "student statistics excemptions scored", "required", array(
                'required' => 'student statistics excemptions scored cannot be empty',
            ));
            $this->form_validation->set_rules("why_ca_exam_series_content", "why ca exam series content", "required", array(
                'required' => 'why ca exam series content cannot be empty',
            ));
            $this->form_validation->set_rules("why_ca_exam_series_embaded_link", "why ca exam series embaded link", "required", array(
                'required' => 'why ca exam series embaded link cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {

            } else {
                if ($_FILES['benefits_we_offer_image']['name'] != '') {
                    $path = 'uploads/';
                    $image = $path . $this->file_upload_with_specific_path('benefits_we_offer_image', $path);
                } else {
                    $image = $this->data['details']->benefits_we_offer_image;
                }
                $data = array(
                    'benefits_we_offer_image' => $image,
                    'benefits_we_offer_content' => $this->input->post('benefits_we_offer_content'),
                    'student_statistics_opted_till_date' => $this->input->post('student_statistics_opted_till_date'),
                    'student_statistics_qualified_students' => $this->input->post('student_statistics_qualified_students'),
                    'student_statistics_5_star_rated' => $this->input->post('student_statistics_5_star_rated'),
                    'student_statistics_excemptions_scored' => $this->input->post('student_statistics_excemptions_scored'),
                    'why_ca_exam_series_content' => $this->input->post('why_ca_exam_series_content'),
                    'why_ca_exam_series_embaded_link' => $this->input->post('why_ca_exam_series_embaded_link'),
                    'updated_at' => time(),
                );
                $res = $this->{$this->model}->update(1, $data);
                if ($res) {
                    $this->session->set_flashdata('success_message', '"Success!","Added Successfully!"');
                    redirect(base_url() . 'admin/home_page_content');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'admin/home_page_content');
                }
            }
        }
        $this->data['page_title'] = 'Home Page Content';
        $this->data['form_title'] = 'Update';
        $this->data['list'] = $this->{$this->model}->get_data();
        $this->admin_view('home_page_content');
    }

}
