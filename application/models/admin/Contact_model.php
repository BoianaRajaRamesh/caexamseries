<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact_model extends CI_Model {

    private $table = "call_back_request";
    private $table1 = "feedback_from_contact_page";

    function get_data() {
        $this->db->select("call_back_request.*, courses.course as course_title");
        $this->db->join('courses', 'courses.id = call_back_request.course');
        $this->db->where('call_back_request.status', 'active');
        $this->db->order_by("call_back_request.id", "desc");
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
        return [];
    }

    function get_feedback_data() {
        $this->db->select("feedback_from_contact_page.*, courses.course as course_title");
        $this->db->join('courses', 'courses.id = feedback_from_contact_page.course');
        $this->db->where('feedback_from_contact_page.status', 'active');
        $this->db->order_by("feedback_from_contact_page.id", "desc");
        $data = $this->db->get($this->table1);
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
        return [];
    }

    function add($data) {
        $this->db->set($data);
        $this->db->insert($this->table);
        if ($this->db->insert_id()) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    function add_feedback($data) {
        $this->db->set($data);
        $this->db->insert($this->table1);
        if ($this->db->insert_id()) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    function delete($id) {
        $this->db->set('status', 'deleted');
        $this->db->where('id', $id);
        $this->db->update($this->table);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return FALSE;
    }

    function delete_feedback($id) {
        $this->db->set('status', 'deleted');
        $this->db->where('id', $id);
        $this->db->update($this->table1);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return FALSE;
    }

}
