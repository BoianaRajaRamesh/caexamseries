<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>admin/dashboard">Dashboard</a>
            </li>
            <li>
                <a href="<?= base_url(); ?>admin/course">Courses</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="" enctype="multipart/form-data" autocomplete="off">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Course Title</label>
                                <div class="col-sm-12">
                                    <input type="text" name="course" class="form-control" required="" value="<?php if (isset($details)) echo $details->course ?>" placeholder="Course Title">
                                </div>
                                <?php echo form_error('course', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">E-Brochure</label>
                                <div class="col-sm-12">
                                    <input type="file" name="brochure" class="form-control" id="brochure" onchange="check_file_size_pdf(this.id)">
                                    <?php if (isset($details)) { ?>
                                        <span><b><a target="_blank" href="<?= base_url($details->brochure); ?>">Previous brochure</a></b></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">Status</label>
                                <div class="col-md-12">
                                    <div class="radio-list">
                                        <label class="radio-inline p-0">
                                            <div class="radio radio-success">
                                                <input name="status" id="radio1" value="active" type="radio" <?= (isset($details->status) && ($details->status == 'active')) ? 'checked' : '' ?> required>
                                                <label for="radio1">Active</label>
                                            </div>
                                        </label>
                                        <label class="radio-inline">
                                            <div class="radio radio-danger">
                                                <input name="status" id="radio2" value="inactive" type="radio" <?= (isset($details->status) && ($details->status == 'inactive')) ? 'checked' : '' ?> required>
                                                <label for="radio2">Inactive</label>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="col-sm-12"> &nbsp; </label>
                            <input type="submit" name="submit" class="btn btn-primary btn-block" value="<?= $page_title; ?>">
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/includes/footer'); ?>
<script>
    function check_file_size_pdf(val) {

        var uploadField = document.getElementById(val);
        var FileName = uploadField.files[0].name;
        var FileExtension = FileName.split('.')[FileName.split('.').length - 1]; //alert(FileExtension);
        if (!(FileExtension == 'pdf' || FileExtension == 'PDF')) {
            swal({
                title: "Warning!",
                text: "Please Upload PDF Files Only",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                closeOnConfirm: false
            });
            document.getElementById(val).value = "";
        } else if (uploadField.files[0].size > 10000000) {
            swal({
                title: "Warning!",
                text: "Max upload size 10 MB",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                closeOnConfirm: false
            });
            document.getElementById(val).value = "";
        }
    }

</script>