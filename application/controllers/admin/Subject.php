<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class Subject extends MY_Controller {

    private $model = "Subject_model";

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/' . $this->model);
        $this->data['unique_name'] = 'courses';
        $this->data['sub_unique_name'] = 'subject';
    }

    function index() {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("subject", "subject", "required|is_unique[subjects.subject]", array(
                'required' => 'subject cannot be empty',
                "is_unique" => "subject must be unique.",
            ));
            $this->form_validation->set_rules("amount", "amount", "required", array(
                'required' => 'amount cannot be empty',
            ));
            $this->form_validation->set_rules("status", "Status", "required", array(
                'required' => 'Status cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {

            } else {
                $data = array(
                    'subject' => $this->input->post('subject'),
                    'amount' => $this->input->post('amount'),
                    'status' => $this->input->post('status'),
                    'created_at' => time(),
                );
                $res = $this->{$this->model}->add($data);
                if ($res) {
                    $this->session->set_flashdata('success_message', '"Success!","Added Successfully!"');
                    redirect(base_url() . 'admin/subject');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'admin/subject');
                }
            }
        }
        $this->data['page_title'] = 'Subjects';
        $this->data['form_title'] = 'Add';
        $this->data['list'] = $this->{$this->model}->get_data();
        $this->admin_view('subject');
    }

    function update($id) {
        if ((isset($id)) && ($id != '')) {
            $this->data['details'] = $this->{$this->model}->get_details($id);

            if ($this->input->post('submit')) {
                if ($this->data['details']->subject == $this->input->post('subject')) {
                    $this->form_validation->set_rules("subject", "subject", "required", array(
                        'required' => 'subject cannot be empty',
                    ));
                } else {
                    $this->form_validation->set_rules("subject", "subject", "required|is_unique[subjects.subject]", array(
                        'required' => 'subject cannot be empty',
                        "is_unique" => "subject must be unique.",
                    ));
                }
                $this->form_validation->set_rules("amount", "amount", "required", array(
                    'required' => 'amount cannot be empty',
                ));
                $this->form_validation->set_rules("status", "Status", "required", array(
                    'required' => 'Status cannot be empty',
                ));
                if ($this->form_validation->run() == FALSE) {

                } else {
                    $data = array(
                        'subject' => $this->input->post('subject'),
                        'amount' => $this->input->post('amount'),
                        'status' => $this->input->post('status'),
                        'updated_at' => time(),
                    );
                    $res = $this->{$this->model}->update($id, $data);
                    if ($res) {
                        $this->session->set_flashdata('success_message', '"Success!","Updated Successfully!"');
                        redirect(base_url() . 'admin/subject');
                    } else {
                        $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                        redirect(base_url() . 'admin/subject/update/' . $id);
                    }
                }
            }
            $this->data['page_title'] = 'Subjects';
            $this->data['form_title'] = 'Update';
            $this->data['list'] = $this->{$this->model}->get_data();
            $this->admin_view('subject');
        }
    }

    function delete($id) {
        if ((isset($id)) && ($id != '')) {
            if ($this->{$this->model}->delete($id)) {
                $this->session->set_flashdata('success_message', "'Deleted Successfully', 'Success'");
                redirect(base_url() . 'admin/subject');
            } else {
                $this->session->set_flashdata('error_message', "'Please Try Again', 'Error'");
                redirect(base_url() . 'admin/subject');
            }
        } else {
            redirect(base_url() . 'admin/subject');
        }
    }

}
