<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

    private $model = "Dashboard_model";

    function __construct() {
        parent::__construct();
        $this->user_login_required();
        $this->load->model($this->model);
    }

    public function index() {
        $this->data['page'] = 'Dashboard';
        $this->web_view('dashboard', $this->data);
    }

}
