<main>
    <div class="contact_info">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="bg_color_11">
                        <div class="contact-form formnew">
                            <div class="contacr-titles">
                                <h1>Request a call back</h1>
                            </div>
                            <div id="message-contact"></div>
                            <form method="post" action="<?= base_url(); ?>contact/call_back_request">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" required name="name" placeholder="Full Name" class="form-control">
                                            <?php echo form_error('name', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- /row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="email" required name="email" placeholder="Email Id" class="form-control">
                                            <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="tel" required name="phone_number" placeholder="Phone Number" class="form-control number" min="10" maxlength="10">
                                            <?php echo form_error('phone_number', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select class="form-control" required name="course">
                                                <option value="">Select Course</option>
                                                <?php foreach ($courses_list as $list) { ?>
                                                    <option value="<?= $list->id; ?>"><?= $list->course; ?></option>
                                                <?php } ?>
                                            </select>
                                            <?php echo form_error('course', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select class="form-control" required name="reason">
                                                <option value="">Select Reason </option>
                                                <option>To Know about CA Exam Series</option>
                                                <option>To Know about Plans & Pricing</option>
                                                <option>To Know about Payment Procedure</option>
                                                <option>To Know about Registration Process</option>
                                                <option>Other</option>
                                            </select>
                                            <?php echo form_error('reason', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <textarea name="feedback" required class="form-control" placeholder="Feedback" rows="4"></textarea>
                                            <?php echo form_error('feedback', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <p class="add_top_30"><input type="submit" name="submit" value="Submit" class="btn btn-warning rounded" id="submit-contact"></p>
                            </form>
                        </div>
                        <div class="contact-feedform formnew">
                            <div class="contacr-titles">
                                <h1>Feedback </h1>
                            </div>
                            <div id="message-contact"></div>
                            <form method="post" action="<?= base_url(); ?>contact/feedback">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="text" required name="fname" placeholder="Name" class="form-control">
                                            <?php echo form_error('fname', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <!-- /row -->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="email" required name="femail" placeholder="Email" class="form-control">
                                            <?php echo form_error('femail', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <input type="tel" required name="fphone_number" placeholder="Mobile" class="form-control number" min="10" maxlength="10">
                                            <?php echo form_error('fphone_number', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <select required class="form-control" name="fcourse">
                                                <option value="">Select Course</option>
                                                <?php foreach ($courses_list as $list) { ?>
                                                    <option value="<?= $list->id; ?>"><?= $list->course; ?></option>
                                                <?php } ?>
                                            </select>
                                            <?php echo form_error('fcourse', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group" style="margin-bottom: 25.5px">
                                            <textarea required name="ffeedback" maxlength="1500" class="form-control" placeholder="Feedback" rows="6"></textarea>
                                            <?php echo form_error('ffeedback', '<div class="error">', '</div>'); ?>
                                        </div>
                                    </div>
                                </div>
                                <p class="add_top_30">
                                    <input type="submit" class="btn btn-warning rounded" name="submit" value="Submit" id="submit-contact">
                                </p>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <ul class="clearfix">
                        <li>
                            <i class="pe-7s-mail-open-file" style="font-size:46px"></i>
                            <h4>Email address</h4>
                            <span> <?php echo $site_settings->support_mail; ?><br> <?php echo $site_settings->alternate_mail; ?>
                            </span>
                        </li>
                        <li>
                            <i class="pe-7s-phone" style="font-size:46px"></i>
                            <h4>Contacts info</h4>
                            <span><i class="fal fa-phone" style="font-size:inherit"></i> <?php echo $site_settings->support_number; ?><br> <i class="fab fa-whatsapp" style="font-size:inherit"></i>  <?php echo $site_settings->alternate_number; ?></span>
                        </li>
                    </ul>
                    <div class="row">
                        <div class="col-lg-8 col-md-12 offset-lg-2">
                            <ul class="contactsocial">
                                <li><a href="<?php echo $site_settings->facebook; ?>" target="_blank"><i class="ti-facebook"></i></a></li>
                                <li><a href="<?php echo $site_settings->twitter; ?>" target="_blank"><i class="ti-twitter-alt"></i></a></li>
                                <li><a href="<?php echo $site_settings->linked_in; ?>" target="_blank"><i class="ti-instagram"></i></a></li>
                                <li><a href="<?php echo $site_settings->youtube; ?>" target="_blank"><i class="ti-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>


<!-- /main -->
<?php
$this->load->view('website/includes/footer');
?>