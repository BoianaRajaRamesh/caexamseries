<main>
    <div class="bg_color_1">
        <div class="container margin_0_35" style="padding:50px 0px;">
            <div class="row justify-content-center">
                <div class="col-lg-6 d-none d-sm-block">
                    <img src="cmoon_images/forgot.png" style="width:100%; margin-top:25px;"/>
                </div>
                <div class="col-lg-4">
                    <div class="contact-form">
                        <div class="contacr-titles">
                            <i class="icon-reset" style="font-size:50px; color:#ec1f26"></i>
                            <h4>FORGOT PASSWORD</h4>
                        </div> <hr>
                        <div id="message-contact"></div>
                        <form action="<?= base_url(); ?>login/update_password" method="POST" >
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="email" name="email" placeholder="Enter Your Valid Email Id" class="form-control" required="">
                                    </div>
                                </div>
                            </div>
                            <p class="add_top_10" align="center">
                                <input type="submit" value="Submit" name="submit" class="btn_1 rounded" id="submit-contact">
                            </p>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /row -->

        </div>
        <!-- /container -->
    </div>
</main>
<!-- /main -->
<?php
$this->load->view('website/includes/footer');
?>