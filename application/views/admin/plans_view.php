<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>admin/dashboard">Dashboard</a>
            </li>
            <li>
                <a href="<?= base_url(); ?>admin/plans">Plans</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <style>b{font-size: 15px}b small{font-size: 14px; font-weight:600}</style>
                    <div class="col-md-3">
                        <b>Title: <small><?= $details->plan_title; ?></small></b>
                    </div>
                    <div class="col-md-3">
                        <b>Actual Price: <small><?= $details->actual_price; ?></small></b>
                    </div>
                    <div class="col-md-3">
                        <b>Discount Price: <small><?= $details->discount_price; ?></small></b>
                    </div>
                    <div class="col-md-3">
                        <b>Exam: <small><?= $details->exam_title; ?></small></b>
                    </div>
                    <div class="col-md-12"><br></div>
                    <div class="col-md-3">
                        <b>Exam Mode: <small><?= $details->exam_mode; ?></small></b>
                    </div>
                    <div class="col-md-3">
                        <b>Subject Wise: <small class="text-capitalize"><?= $details->subject_wise; ?></small></b>
                    </div>
                    <div class="col-md-3">
                        <b>Status: <small class="text-capitalize"><?= $details->status; ?></small></b>
                    </div>
                    <div class="col-md-3">
                        <b>Schedule:
                            <a href="<?= base_url($details->schedule_pdf) ?>" target="_blank">
                                <i class="fa fa-file-pdf-o fa-2x"></i>
                            </a></b>
                    </div>
                    <div class="col-md-12">
                        <b>Subjects List</b>
                    </div>

                    <?php foreach ($details->plan_wise_subjects as $row) {
                        ?>
                        <div class="col-md-12">
                            <b><small class="text-capitalize"><?= $this->db->where('id', $row->subject_id)->get('subjects')->row()->subject; ?></small></b>
                        </div>
                    <?php } ?>
                    <div class="col-md-12">
                        <br>
                        <b>Points List</b>
                    </div>
                    <?php foreach ($details->plan_wise_points as $row) {
                        ?>
                        <div class="col-md-12">
                            <b><small class="text-capitalize"><?= $row->description; ?></small></b>
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/includes/footer'); ?>
