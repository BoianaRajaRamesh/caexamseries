<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Courses_model extends CI_Model {

    function exams_list() {
        $this->db->order_by('id', 'asc');
        return $this->db->get('exams')->result();
    }

    function get_plans_list($exam_id, $course_id) {
        $this->db->where('exam_id', $exam_id);
        $this->db->where('course_id', $course_id);
        $this->db->where('status', 'active');
        $data = $this->db->get('plans')->result();
        foreach ($data as $row) {
            $row->points = $this->db->where('plan_id', $row->id)->get('plan_wise_points')->result();
            $row->subjects = $this->db->select('subjects.subject, subjects.amount,plan_wise_subjects.* ')->join('subjects', 'subjects.id = plan_wise_subjects.subject_id')->where('plan_wise_subjects.plan_id', $row->id)->get('plan_wise_subjects')->result();
        }
        return $data;
    }

    function get_active_banners($id) {
        $this->db->where('status', '1');
        $this->db->where('course_id', $id);
        $this->db->order_by("priority", "asc");
        $data = $this->db->get('courses_banners');
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
        return [];
    }

}
