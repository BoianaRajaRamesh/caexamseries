<main>

    <section id="hero_in" class="general">

        <div class="wrapper">

            <div class="container">

                <h1 class="fadeInUp"><span></span>OTP</h1>

            </div>

        </div>

    </section>

    <div class="bg_color_1">

        <div class="container margin_0_35" style="padding:50px 0px;">

            <div class="row justify-content-center">

                <div class="col-lg-6 d-none d-sm-block">



                    <img src="<?= base_url(); ?>cmoon_images/login.png" style="width:100%; margin-top:25px;"/>

                </div>

                <div class="col-lg-4">

                    <div class="contact-form">

                        <div class="contacr-titles">

                            <i class="icon-key" style="font-size:50px; color:#ec1f26"></i>

                            <h4>OTP</h4>

                        </div>

                        <div id="message-contact"></div>

                        <form method="POST" enctype='multipart/form-data' action="<?php echo base_url(); ?>login/otp_verify" autocomplete="off">

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">

                                        <input type="text" name="otp" placeholder="Please Enter OTP" class="form-control" required>

                                    </div>
                                    <?php echo form_error('otp', '<div class="error">', '</div>'); ?>
                                </div>

                            </div>


                            <!-- /row -->

                            <p class="add_top_10" align="center">
                                <input type="submit" name="submit" value="submit" class="btn_1 rounded" id="submit-contact">
                            </p>
                            <div class="row">
                                <div class="col-md-6 col-6 text-left">
                                    <span><a href="<?= base_url(); ?>login/change_number">Change Your Number (<?= $this->session->userdata('web_mobile_number') ?>)</a></span>
                                </div>
                                <div class="col-md-6 col-6 text-right">
                                    <span><a href="<?= base_url(); ?>login/resend_otp">Resend OTP</a></span>
                                </div>
                            </div>
                        </form>

                    </div>

                </div>



            </div>

            <!-- /row -->





        </div>

        <!-- /container -->

    </div>



</main>
<?php
$this->load->view('website/includes/footer');
?>