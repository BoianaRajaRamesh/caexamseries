<main>

    <div class="bg_color_1">

        <div class="container margin_0_35" style="padding:50px 0px;">

            <div class="row justify-content-center">

                <div class="col-lg-6 d-none d-sm-block">

                    <img src="<?= base_url(); ?>cmoon_images/login.png" style="width:100%; margin-top:25px;"/>

                </div>

                <div class="col-lg-4">

                    <div class="contact-form">

                        <div class="contacr-titles">

                            <i class="icon-user" style="font-size:50px; color:#ec1f26"></i>

                            <h4>Login</h4>

                        </div>

                        <div id="message-contact"></div>

                        <form method="post" enctype="multipart/form-data" action="<?= base_url(); ?>login/login_user">

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">

                                        <input type="text" name="username" placeholder="Phone Number/Email" class="form-control" required>

                                    </div>

                                </div>

                            </div>

                            <!-- /row -->

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">

                                        <input type="password" name="password" placeholder="Password" class="form-control" required>
                                        <div class="col-md-12 col-12 text-right">
                                            <span><a href="<?= base_url(); ?>login/forgot_password">Forgot password?</a></span>
                                        </div>
                                    </div>

                                </div>



                            </div>
                            <input type="submit" name="submit" value="Login" align="center" class="add_top_10 btn_1 rounded" />

                            <div class="form-group">

                                <div class="col-md-12 control">

                                    <div style="border-top: 1px solid#888; padding-top:5px; font-size:85%" >
                                        Don’t have an account
                                        <a href="<?= base_url(); ?>login/register">
                                            Sign Up
                                        </a>
                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>



            </div>

            <!-- /row -->


        </div>

        <!-- /container -->

    </div>


</main>


<?php
$this->load->view('website/includes/footer');
?>