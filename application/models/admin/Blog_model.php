<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Blog_model extends CI_Model {

    private $table = "blogs";

    function get_data() {
        $this->db->order_by("id", "desc");
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
    }

    function get_active_data() {
        $this->db->where('status', 'active');
        $this->db->order_by("id", "desc");
        $this->db->limit(10);
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
    }

    function get_active_popular_data() {
        $this->db->where('status', 'active');
        $this->db->where('popular', 'yes');
        $this->db->order_by("id", RAND());
        $this->db->limit(3);
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->result();
            return $result;
        }
    }

    function get_details($id) {
        $this->db->where('id', $id);
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->row();
            return $result;
        }
    }

    function add($data) {
        $this->db->set($data);
        $this->db->insert($this->table);
        if ($this->db->insert_id()) {
            return $this->db->insert_id();
        }
    }

    function update($id, $data) {
        $this->db->set($data);
        $this->db->where('id', $id);
        if ($this->db->update($this->table)) {
            return true;
        }
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete($this->table);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
    }

}
