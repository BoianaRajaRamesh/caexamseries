<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends MY_Controller {

    private $model = "Course_model";

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/' . $this->model);
        $this->data['unique_name'] = 'courses';
        $this->data['sub_unique_name'] = 'course';
    }

    function index() {
        $this->data['page_title'] = 'Courses';
        $this->data['form_title'] = 'Add';
        $this->data['list'] = $this->{$this->model}->get_data();
        $this->admin_view('course');
    }

    function add() {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("course", "course", "required|is_unique[courses.course]", array(
                'required' => 'course cannot be empty',
                "is_unique" => "course must be unique.",
            ));
            $this->form_validation->set_rules("status", "Status", "required", array(
                'required' => 'Status cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {

            } else {
                $path = 'uploads/courses/';
                $image = $this->file_upload_with_specific_path('brochure', $path);
                $data = array(
                    'course' => $this->input->post('course'),
                    'status' => $this->input->post('status'),
                    'created_at' => time(),
                    'brochure' => $path . $image,
                );
                $res = $this->{$this->model}->add($data);
                if ($res) {
                    $this->session->set_flashdata('success_message', '"Success!","Added Successfully!"');
                    redirect(base_url() . 'admin/course');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'admin/course');
                }
            }
        }
        $this->data['page_title'] = 'Add Course';
        $this->data['subjects'] = $this->{$this->model}->get_subjects();
        $this->admin_view('course_add');
    }

    function update($id) {
        if ((isset($id)) && ($id != '')) {
            $this->data['details'] = $this->{$this->model}->get_details($id);

            if ($this->input->post('submit')) {
                if ($this->data['details']->course == $this->input->post('course')) {
                    $this->form_validation->set_rules("course", "course", "required", array(
                        'required' => 'course cannot be empty',
                    ));
                } else {
                    $this->form_validation->set_rules("course", "course", "required|is_unique[courses.course]", array(
                        'required' => 'course cannot be empty',
                        "is_unique" => "course must be unique.",
                    ));
                }
                $this->form_validation->set_rules("status", "Status", "required", array(
                    'required' => 'Status cannot be empty',
                ));
                if ($this->form_validation->run() == FALSE) {

                } else {
                    if ($_FILES['brochure']['name'] != '') {
                        $path = 'uploads/courses/';
                        $image = $path . $this->file_upload_with_specific_path('brochure', $path);
                    } else {
                        $image = $this->data['details']->brochure;
                    }
                    $data = array(
                        'course' => $this->input->post('course'),
                        'status' => $this->input->post('status'),
                        'created_at' => time(),
                        'brochure' => $image,
                    );
                    $res = $this->{$this->model}->update($id, $data);
                    if ($res) {
                        $this->session->set_flashdata('success_message', '"Success!","Updated Successfully!"');
                        redirect(base_url() . 'admin/course');
                    } else {
                        $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                        redirect(base_url() . 'admin/course/update/' . $id);
                    }
                }
            }
            $this->data['page_title'] = 'Update Course';
            $this->data['subjects'] = $this->{$this->model}->get_subjects();
            $this->admin_view('course_add');
        }
    }

    function delete($id) {
        if ((isset($id)) && ($id != '')) {
            if ($this->{$this->model}->delete($id)) {
                $this->session->set_flashdata('success_message', "'Deleted Successfully', 'Success'");
                redirect(base_url() . 'admin/course');
            } else {
                $this->session->set_flashdata('error_message', "'Please Try Again', 'Error'");
                redirect(base_url() . 'admin/course');
            }
        } else {
            redirect(base_url() . 'admin/course');
        }
    }

}
