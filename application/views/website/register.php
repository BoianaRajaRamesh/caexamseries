<main>
    <div class="bg_color_1">

        <div class="container margin_0_35" style="padding:50px 0px;">

            <div class="row justify-content-center">

                <div class="col-lg-6 d-none d-sm-block">

                    <img src="<?= base_url(); ?>cmoon_images/login.png" style="width:100%; margin-top:25px;"/>

                </div>

                <div class="col-lg-4">

                    <div class="contact-form">

                        <div class="contacr-titles">

                            <i class="icon-users" style="font-size:50px; color:#ec1f26"></i>

                            <h4>REGISTER</h4>

                        </div>

                        <div id="message-contact"></div>

                        <form method="post" enctype="multipart/form-data" action="<?= base_url(); ?>login/register_user">

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">

                                        <input type="text" name="name" placeholder="User Name" class="form-control" required>

                                    </div>
                                    <?php echo form_error('name', '<div class="error">', '</div>'); ?>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">

                                        <input type="text" name="email" placeholder="Email" class="form-control" required>

                                    </div>
                                    <?php echo form_error('email', '<div class="error">', '</div>'); ?>
                                </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">

                                        <input type="text" name="phone_number" placeholder="Phone Number" class="form-control" required>

                                    </div>
                                    <?php echo form_error('phone_number', '<div class="error">', '</div>'); ?>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <input type="text" name="icia_rege_no" placeholder="ICAI Reg No" class="form-control">
                                    </div>
                                    <?php echo form_error('icia_rege_no', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <select class="form-control" name="course">
                                            <option value="">Select Course</option>
                                            <?php foreach ($courses_list as $list) { ?>
                                                <option value="<?= $list->id; ?>"><?= $list->course; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <?php echo form_error('course', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                            <!-- /row -->

                            <div class="row">

                                <div class="col-md-12">

                                    <div class="form-group">
                                        <input type="password" name="password" placeholder="Password" class="form-control" required>
                                    </div>
                                    <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                                </div>

                            </div>

                            <input name="submit" type="submit" value="Register" class="add_top_10 btn_1 rounded" />
                            <div class="form-group">

                                <div class="col-md-12 control">

                                    <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >

                                        Already have an account!

                                        <a href="<?= base_url(); ?>login">
                                            Sign In
                                        </a>

                                    </div>

                                </div>

                            </div>

                        </form>

                    </div>

                </div>



            </div>

            <!-- /row -->


        </div>

        <!-- /container -->

    </div>


</main>

<?php
$this->load->view('website/includes/footer');
?>