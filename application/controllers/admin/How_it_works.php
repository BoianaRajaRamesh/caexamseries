<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class How_it_works extends MY_Controller {

    private $model = "How_it_works_model";

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/' . $this->model);
        $this->data['unique_name'] = 'masters';
        $this->data['sub_unique_name'] = 'how_it_works';
    }

    function index() {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("title", "title", "required|is_unique[how_it_works.title]", array(
                'required' => 'title cannot be empty',
                "is_unique" => "title must be unique.",
            ));
            $this->form_validation->set_rules("icon", "icon", "required", array(
                'required' => 'icon cannot be empty',
            ));
            $this->form_validation->set_rules("priority", "priority", "required", array(
                'required' => 'priority cannot be empty',
            ));
            $this->form_validation->set_rules("status", "Status", "required", array(
                'required' => 'Status cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {

            } else {
                $data = array(
                    'title' => $this->input->post('title'),
                    'icon' => $this->input->post('icon'),
                    'priority' => $this->input->post('priority'),
                    'status' => $this->input->post('status'),
                    'created_at' => time(),
                );
                $res = $this->{$this->model}->add($data);
                if ($res) {
                    $this->session->set_flashdata('success_message', '"Success!","Added Successfully!"');
                    redirect(base_url() . 'admin/how_it_works');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'admin/how_it_works');
                }
            }
        }
        $this->data['page_title'] = 'How It Works';
        $this->data['form_title'] = 'Add';
        $this->data['list'] = $this->{$this->model}->get_data();
        $this->admin_view('how_it_works');
    }

    function update($id) {
        if ((isset($id)) && ($id != '')) {
            $this->data['details'] = $this->{$this->model}->get_details($id);

            if ($this->input->post('submit')) {
                if ($this->data['details']->link == $this->input->post('link')) {
                    $this->form_validation->set_rules("title", "title", "required", array(
                        'required' => 'title cannot be empty',
                    ));
                } else {
                    $this->form_validation->set_rules("title", "title", "required|is_unique[how_it_works.title]", array(
                        'required' => 'title cannot be empty',
                        "is_unique" => "title must be unique.",
                    ));
                }

                $this->form_validation->set_rules("icon", "icon", "required", array(
                    'required' => 'icon cannot be empty',
                ));
                $this->form_validation->set_rules("priority", "priority", "required", array(
                    'required' => 'priority cannot be empty',
                ));
                $this->form_validation->set_rules("status", "Status", "required", array(
                    'required' => 'Status cannot be empty',
                ));
                if ($this->form_validation->run() == FALSE) {

                } else {
                    $data = array(
                        'title' => $this->input->post('title'),
                        'icon' => $this->input->post('icon'),
                        'priority' => $this->input->post('priority'),
                        'status' => $this->input->post('status'),
                        'updated_at' => time(),
                    );
                    $res = $this->{$this->model}->update($id, $data);
                    if ($res) {
                        $this->session->set_flashdata('success_message', '"Success!","Updated Successfully!"');
                        redirect(base_url() . 'admin/how_it_works');
                    } else {
                        $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                        redirect(base_url() . 'admin/how_it_works/update/' . $id);
                    }
                }
            }
            $this->data['page_title'] = 'How It Works';
            $this->data['form_title'] = 'Update';
            $this->data['list'] = $this->{$this->model}->get_data();
            $this->admin_view('how_it_works');
        }
    }

    function delete($id) {
        if ((isset($id)) && ($id != '')) {
            if ($this->{$this->model}->delete($id)) {
                $this->session->set_flashdata('success_message', "'Deleted Successfully', 'Success'");
                redirect(base_url() . 'admin/how_it_works');
            } else {
                $this->session->set_flashdata('error_message', "'Please Try Again', 'Error'");
                redirect(base_url() . 'admin/how_it_works');
            }
        } else {
            redirect(base_url() . 'admin/how_it_works');
        }
    }

}
