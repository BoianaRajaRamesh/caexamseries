<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-md-4">
                <h5>Stay <span>Connected</span></h5>
                <p class="mb-0 pb-1">Subscribe To Our Newsletter</p>
                <form method="post" action="<?= base_url(); ?>home/subscribe">
                    <div class="input-group subscribe">
                        <input type="email" class="form-control" name="mail_id" placeholder="Enter Email Address"/>
                        <div class="input-group-append">
                            <button class="btn btn-warning" type="submit" name="submit" value="submit" id="button-addon2"><i class="fal fa-paper-plane"></i></button>
                        </div>
                    </div><hr>
                </form>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <h5>Keep Me <span>Touch</span></h5>
                        <div class="follow_us">
                            <ul>
                                <li><a href="<?php echo $site_settings->facebook; ?>" target="_blank"><i class="ti-facebook"></i></a></li>
                                <li><a href="<?php echo $site_settings->twitter; ?>" target="_blank"><i class="ti-twitter-alt"></i></a></li>
                                <li><a href="<?php echo $site_settings->linked_in; ?>" target="_blank"><i class="ti-instagram"></i></a></li>
                                <li><a href="<?php echo $site_settings->youtube; ?>" target="_blank"><i class="ti-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8">
                <div class="row">

                    <div class="col-lg-4 col-md-6  col-sm-6 ml-lg-auto col-6">
                        <h5>Quick <span>links</span></h5>
                        <ul>
                            <li><a href="<?php echo base_url(); ?>index">Home</a></li>
                            <li><a href="<?php echo base_url(); ?>pass_guarantee">Pass Gurantee</a></li>
                            <li><a href="<?php echo base_url(); ?>blog">Blog</a></li>
                            <li><a href="<?php echo base_url(); ?>contact">Contact Us</a></li>
                            <li><a href="<?php echo base_url(); ?>view/register">Free MCQ</a></li>

                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 col-6">
                        <h5>Contact <span>Us</span></h5>
                        <ul class="contacts">
                            <li><a href="tel:<?php echo $site_settings->support_number; ?>"><i class="fal fa-phone"></i> <?php echo $site_settings->support_number; ?></a></li>
                            <li><a href="mailto:<?php echo $site_settings->support_mail; ?>"><i class="fal fa-envelope"></i> <?php echo $site_settings->support_mail; ?></a></li>
                            <li><a href="https://api.whatsapp.com/send?phone=<?php echo $site_settings->alternate_number; ?>" target='_blank'><i class="fab fa-whatsapp"></i> <?php echo $site_settings->alternate_number; ?></a></li>

                        </ul>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-6 security">
                        <h5>Security &amp; <span>Verified</span></h5>
                        <img src="<?php echo base_url(); ?>site_assets/images/trustlogos.png" alt="" height="90">
                        <div class="row mt-3">
                            <div class="col-lg-12 col-md-12 payment">
                                <h5>We <span>Accept</span></h5>
                                <img src="<?php echo base_url(); ?>site_assets/images/paymentcards.jpg" class="img-fluid">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div id="copy">© 2020 caexamseries.co.in Copyright All Rights Reserved  I Made with <i class="fal fa-heart"></i> By <a href="https://thecolourmoon.com/" target="_blank">Colourmoon</a></div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <ul class="prvicylinks">
                        <li><a href="<?php echo base_url(); ?>privacy_policy">Privacy Policy</a></li>
                        <li><a href="<?php echo base_url(); ?>terms">Terms and Conditions</a></li>
                        <li><a href="<?php echo base_url(); ?>refund">Retruns & Refund</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
</div>
<div class="infobar d-block d-md-none">
    <ul class="nav align-items-center justify-content-center">
        <li class="nav-item"><a href="https://api.whatsapp.com/send?phone=+919603960346" target="_blank" class="nav-link"><i class="fab fa-whatsapp"></i> <small>Whatsapp</small></a></li>
        <li class="nav-item"><a href="tel:+919603960346" target="_blank" class="nav-link"><i class="fal fa-phone"></i> <small>Call Us</small></a></li>
        <li class="nav-item"><a href="mailto:info@caexamseries.co.in" target="_blank" class="nav-link"><i class="fal fa-envelope-open"></i> <small>Email Us</small></a></li>
        <li class="nav-item"><a href="javascript:void(Tawk_API.toggle());" class="nav-link"><i class="fal fa-comment-alt-plus"></i> <small>Chat Now</small></a></li>
    </ul>
</div>
<!-- page -->
<!-- COMMON SCRIPTS -->
<script src="<?php echo base_url(); ?>/site_assets/js/common_scripts.js"></script>
<script src="<?php echo base_url(); ?>/site_assets/js/main.js"></script>
<script src="<?php echo base_url(); ?>/site_assets/js/validate.js"></script>
<!-- SPECIFIC SCRIPTS -->
<script src="<?php echo base_url(); ?>/site_assets/layerslider/js/greensock.js"></script>
<script src="<?php echo base_url(); ?>/site_assets/layerslider/js/layerslider.transitions.js"></script>
<script src="<?php echo base_url(); ?>/site_assets/layerslider/js/layerslider.kreaturamedia.jquery.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/site_assets/css/intlTelInput.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>/site_assets/js/intlTelInput.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/site_assets/js/rating.js"></script>
<!-- <script src="<?php echo base_url(); ?>/site_assets/js/jquery.side-slider.js"></script> -->
<script src="//cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>

<script src="<?= base_url() ?>admin_assets/js/plugins/toastr/toastr.min.js"></script>
<script>
    $(document).ready(function () {
        setTimeout(function () {
            toastr.options = {closeButton: true,
                progressBar: true,
                showMethod: 'slideDown',
                timeOut: 4000
            };
<?php if ($this->session->flashdata('success_message')) { ?>
                toastr.success(<?= $this->session->flashdata('success_message') ?>);
<?php } ?>
<?php if ($this->session->flashdata('error_message')) { ?>
                toastr.error(<?= $this->session->flashdata('error_message') ?>);
<?php } ?>
<?php if ($this->session->flashdata('warning_message')) { ?>
                toastr.warning(<?= $this->session->flashdata('warning_message') ?>);
<?php } ?>
<?php /* <?= ((validation_errors()) && (null !== validation_errors())) ? "toastr.warning('".removeNewLine(removepTag(validation_errors()))."');":"" */ ?>
        }, 1300);
        //Date picker script starts here
    });
    $(function () {
        $('.number').on('keydown', function (e) {
            -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || /65|67|86|88/.test(e.keyCode) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault()
        });
    });
</script>
<?php /* if ($this->session->tempdata('success')) { ?>
  <script>
  alert('<?= $this->session->tempdata('success') ?>');
  </script>
  <?php } */ ?>
<?php /* if ($this->session->tempdata('error')) { ?>
  <script>
  alert('<?= $this->session->tempdata('error') ?>');
  </script>
  <?php } */ ?>
<script>
    $(".phone").intlTelInput({
        utilsScript: "js/utils.js"
    });
</script>
<script type="text/javascript">
    $(function () {

        $(".rateYo").rateYo({
            rating: 0
        });

    });
</script>
<script type="text/javascript">
    $(function () {
        $(".rateYo").rateYo({
            rating: 0,
            starWidth: "17px",
            ratedFill: "#fad022",
            normalFill: "#efefef",
            onSet: function (rating, rateYoInstance) {
                $('.rating_val').val(rating);
            },
        });
    });
    $(function () {
        $(".rateYo1").each(function () {
            $(this).rateYo({
                starWidth: "17px",
                ratedFill: "#efefef",
                normalFill: "#efefef",
                readOnly: true,
                rating: $(this).data('value')
            });
        });
    });
</script>
<script type="text/javascript">
    'use strict';
    $('#layerslider').layerSlider({
        autoStart: true,
        navButtons: false,
        navStartStop: false,
        showCircleTimer: false,
        responsive: true,
        responsiveUnder: 1280,
        layersContainer: 1200,
        skinsPath: 'layerslider/skins/'
// Please make sure that you didn't forget to add a comma to the line endings
// except the last line!
    });
</script>
<script>
    $(document).ready(function () {
        $("#testimonial-slider").owlCarousel({
            loop: true,
            margin: 10,
            responsiveClass: true,
            autoplay: true,
            dots: true,
            responsive: {
                0: {
                    items: 1,
                    nav: false
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 1,
                    nav: false,
                    loop: true,
                    margin: 20
                }
            }
        });


        $("#main-slider").owlCarousel({
            loop: true,
            items: 1,
            autoplay: true,
            dots: false,
            nav: true,
            navText: ['<i class="fal fa-angle-left"></i>', '<i class="fal fa-angle-right"></i>']
        });

        $("#feedback-slider,#sampleq-slider").owlCarousel({
            loop: true,
            margin: 10,
            items: 4,
            responsiveClass: true,
            autoplay: true,
            dots: false,
            nav: true,
            navText: ['<i class="fal fa-angle-left"></i>', '<i class="fal fa-angle-right"></i>'],
            responsive: {
                0: {
                    items: 1,
                },
                480: {
                    items: 1,
                },
                600: {
                    items: 2,
                },
                1200: {
                    items: 3,
                },
                1300: {
                    items: 4,
                }
            }
        });

    })




</script>
<script type="text/javascript">
    /*$(document).ready(function(){
     $('#sideslider').sideSlider();
     });*/

</script>
<script>
    function goBack() {
        window.history.back()
    }

    $('.count-this').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 2000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });






// Prevent closing from click inside dropdown
    $(document).on('click', '.dropdown-menu', function (e) {
        e.stopPropagation();
    });

// make it as accordion for smaller screens
    if ($(window).width() < 992) {
        $('.dropdown-menu a').click(function (e) {
            e.preventDefault();
            if ($(this).next('.submenu').length) {
                $(this).next('.submenu').toggle();
            }
            $('.dropdown').on('hide.bs.dropdown', function () {
                $(this).find('.submenu').hide();
            })
        });
    }


    var originalPos;
    $(document).on("scroll", function (e) {

        if ($(".exam-btns").hasClass("affixed")) {
            if ($(document).scrollTop() <= originalPos)
                $(".exam-btns").removeClass("affixed");
            return;
        }

        if ((originalPos = $(document).scrollTop()) >= ($(".exam-btns").offset().top - $(".exam-btns").height())) {
            $(".exam-btns").addClass("affixed");
        }
    })


</script>

<div class="modal fade" id="add_cart_pop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-center">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Which Mode of Exam You Prefer?</h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fal fa-times-circle"></i></span><span class="sr-only">Close</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row quemcq">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header bg-drkred">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="option1" name="scheduled" class="custom-control-input" >
                                        <label class="custom-control-label custlbl" for="option1">Scheduled</label>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <ul class="schdlist">
                                        <li> Pre-Fixed Schedule</li>
                                        <li> Our Time Table</li>
                                        <li> Write On Same day</li>
                                    </ul>
                                    <a href="#" class="btn btn-secondary btn-block mt-1"><i class="fal fa-download"></i> Download Schedule</a>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-header bg-yellow">
                                    <div class="custom-control custom-radio custom-control-inline">
                                        <input type="radio" id="option2" name="scheduled" class="custom-control-input" >
                                        <label class="custom-control-label custlbl" for="option2">Un-Scheduled</label>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <ul class="schdlist">
                                        <li> All Access</li>
                                        <li> No Time tables</li>
                                        <li> Write @ Any Time</li>
                                    </ul>
                                    <a href="#" class="btn btn-secondary btn-block mt-1"><i class="fal fa-download"></i> Download Syllabus</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 text-center"><h3><i class="fal fa-rupee-sign"></i> 2300/-</h3></div>
                    </div>
                    <div class="row">

                        <div class="col-lg-12">
                            <div class="alert alert-warning text-center" role="alert">
                                <h5>Student should select any mode of exam here</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-lg-12 text-center">
                            <button type="submit" class="btn btn-dark"><i class="fal fa-chevron-circle-right"></i> Continue For Payment</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>





<div class="modal fade" id="answerModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Answer Sheet</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times-circle"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <h4 class="text-center text-success">
                    <i class="fal fa-check-circle fa-2x"></i> <br>
                    No. of Questions <br>Answered Correctly 6/10
                </h4> <hr>
                <h4 class="text-center text-danger">
                    <i class="fal fa-times-circle fa-2x"></i> <br>
                    No. of Questions <br>Answered Wrongly 4/10
                </h4>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="individualModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Individual Subjects & Features</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times-circle"></i></span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card">
                            <div class="card-header bg-drkred text-white">
                                Accounts
                            </div>
                            <div class="card-body">
                                <ul class="schdlist">
                                    <li> 3 Unit Exams</li>
                                    <li> 2 Mock Exam</li>
                                    <li> 100 % Coverage</li>
                                    <li> Systematic Schedule</li>
                                </ul>
                                <h4><a href="#" class="btn btn-secondary btn-block mt-1"><i class="fal fa-rupee-sign"></i> 499</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card">
                            <div class="card-header bg-drkred text-white">
                                Law
                            </div>
                            <div class="card-body">
                                <ul class="schdlist">
                                    <li> 3 Unit Exams</li>
                                    <li> 2 Mock Exam</li>
                                    <li> 100 % Coverage</li>
                                    <li> Systematic Schedule</li>
                                </ul>
                                <h4><a href="#" class="btn btn-secondary btn-block mt-1"><i class="fal fa-rupee-sign"></i> 499</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card">
                            <div class="card-header bg-drkred text-white">
                                Costing & FM
                            </div>
                            <div class="card-body">
                                <ul class="schdlist">
                                    <li> 3 Unit Exams</li>
                                    <li> 2 Mock Exam</li>
                                    <li> 100 % Coverage</li>
                                    <li> Systematic Schedule</li>
                                </ul>
                                <h4><a href="#" class="btn btn-secondary btn-block mt-1"><i class="fal fa-rupee-sign"></i> 499</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card">
                            <div class="card-header bg-drkred text-white">
                                Taxation
                            </div>
                            <div class="card-body">
                                <ul class="schdlist">
                                    <li> 3 Unit Exams</li>
                                    <li> 2 Mock Exam</li>
                                    <li> 100 % Coverage</li>
                                    <li> Systematic Schedule</li>
                                </ul>
                                <h4><a href="#" class="btn btn-secondary btn-block mt-1"><i class="fal fa-rupee-sign"></i> 499</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card">
                            <div class="card-header bg-drkred text-white">
                                Adv.Accounting
                            </div>
                            <div class="card-body">
                                <ul class="schdlist">
                                    <li> 3 Unit Exams</li>
                                    <li> 2 Mock Exam</li>
                                    <li> 100 % Coverage</li>
                                    <li> Systematic Schedule</li>
                                </ul>
                                <h4><a href="#" class="btn btn-secondary btn-block mt-1"><i class="fal fa-rupee-sign"></i> 499</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card">
                            <div class="card-header bg-drkred text-white">
                                Auditing
                            </div>
                            <div class="card-body">
                                <ul class="schdlist">
                                    <li> 3 Unit Exams</li>
                                    <li> 2 Mock Exam</li>
                                    <li> 100 % Coverage</li>
                                    <li> Systematic Schedule</li>
                                </ul>
                                <h4><a href="#" class="btn btn-secondary btn-block mt-1"><i class="fal fa-rupee-sign"></i> 499</a></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="card">
                            <div class="card-header bg-drkred text-white">
                                IT & SM
                            </div>
                            <div class="card-body">
                                <ul class="schdlist">
                                    <li> 3 Unit Exams</li>
                                    <li> 2 Mock Exam</li>
                                    <li> 100 % Coverage</li>
                                    <li> Systematic Schedule</li>
                                </ul>
                                <h4><a href="#" class="btn btn-secondary btn-block mt-1"><i class="fal fa-rupee-sign"></i> 499</a></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
if ($page == 'index') {
    ?>
    <div class="modal fade" id="welcomeModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="fal fa-times-circle"></i>
                    </button>
                    <img src="<?php echo base_url(); ?>site_assets/images/welcome2021.jpg" class="img-fluid"/>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
</body>
</html>
