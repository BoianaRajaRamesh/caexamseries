<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>admin/dashboard">Dashboard</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $form_title; ?> <?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <form action="" method="post" enctype="multipart/form-data">

                        <div class="form-group col-md-4">
                            <label>Image</label>
                            <input class="form-control" onchange="check_file_size(this.id)" id="image" type="file" name="image" <?= (isset($details->id)) ? '' : 'required' ?>>
                            <?php
                            if (isset($details->image) && $details->image != '') {
                                ?>
                                <input type="hidden" name="previous_image" style="height: 100px; width: 100px" value="<?= $details->image ?>"/>
                                <img src="<?= base_url() . $details->image ?>" class="img-responsive"><br>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Name</label>
                            <input class="form-control" required type="text" name="name" placeholder="Name" value="<?= (isset($details->name)) ? $details->name : '' ?>" required="">
                        </div>
                        <div class="form-group col-md-4">
                            <label>Location</label>
                            <input class="form-control" type="text" name="location" required placeholder="Location" value="<?= (isset($details->location)) ? $details->location : '' ?>" required="">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Description</label>
                            <textarea class="form-control" placeholder="Description" required name="description"><?= (isset($details->description)) ? $details->description : '' ?></textarea>
                        </div>

                        <div class="form-group col-md-4">
                            <label class="control-label">Status</label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-success">
                                        <input name="status" id="radio1" value="active" type="radio" <?= (isset($details->status) && ($details->status == 'active')) ? 'checked' : '' ?> required>
                                        <label for="radio1">Active</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-danger">
                                        <input name="status" id="radio2" value="inactive" type="radio" <?= (isset($details->status) && ($details->status == 'inactive')) ? 'checked' : '' ?> required>
                                        <label for="radio2">Inactive</label>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label></label><br>
                            <button type="submit" class="btn btn-rounded px-20 btn-info waves-effect waves-light m-r-10" name="submit" value="submit">Submit</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <!-- table-responsive -->
                    <div class="">
                        <table id="myTable" class="table table-striped">
                            <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Image</th>
                                    <th>Description</th>
                                    <th>Name</th>
                                    <th>Location</th>
                                    <th>Created At</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($list) && (count($list) > 0)) {
                                    $i = 1;
                                    foreach ($list as $item) {
                                        ?>
                                        <tr>
                                            <td><?= $i ?></td>
                                            <td><img src="<?= base_url() . $item->image ?>" class="img-responsive" style="max-width: 150px; max-height: 100px"></td>
                                            <td><?= $item->description ?></td>
                                            <td><?= $item->name ?></td>
                                            <td><?= $item->location ?></td>
                                            <td><?= display_date_and_time_format($item->created_at) ?></td>
                                            <td class="text-capitalize"><?= $item->status ?></td>
                                            <td>
                                                <a href="<?= base_url() ?>admin/testimonial/update/<?= $item->id ?>" class="btn btn-sm btn-circle btn-success"><i class="fa fa-edit"></i></a>
                                                <button type="button" class="btn btn-sm btn-circle btn-danger delete_item" value="<?= $item->id ?>"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php
$this->load->view('admin/includes/footer');
?>
<script type="text/javascript">
    $('#text').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9 ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $(document).ready(function () {
        $('#myTable').DataTable();
        //Delete Confirmation Script starts here
        $(document).on("click", '.delete_item', function () {
            var del_id = $(this).val();
            swal({
                title: "Are you sure?",
                text: "You want to Delete entire data related to this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                window.location = "<?= base_url() ?>admin/testimonial/delete/" + del_id;
            });
        });
        //Delete Confirmation Script ends here
    });
</script>