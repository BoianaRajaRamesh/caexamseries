<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Dashboard_model');
        $this->load->model('admin/Home_page_content_model');
        $this->load->model('admin/How_it_works_model');
        $this->load->model('admin/Feedback_model');
        $this->load->model('admin/Testimonial_model');
        $this->load->model('admin/Faqs_model');
        $this->load->model('admin/Banners_model');
    }

    public function index() {
        $this->data['page'] = 'Home';
        $this->data['banners'] = $this->Banners_model->get_active_banners();
        $this->data['faqs'] = $this->Faqs_model->get_all_active_data();
        $this->data['feedbacks'] = $this->Feedback_model->get_active_data();
        $this->data['testimonials'] = $this->Testimonial_model->get_active_data();
        $this->data['home_content'] = $this->Home_page_content_model->get_details(1);
        $this->data['how_it_works'] = $this->How_it_works_model->get_active_data();
        $this->web_view('home', $this->data);
    }

    function subscribe() {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("mail_id", "mail_id", "required|is_unique[subscribe_mail.mail_id]", array(
                'required' => 'Mail ID cannot be empty',
                "is_unique" => "You have already subscribed",
            ));
            if ($this->form_validation->run() == FALSE) {
                $error_message = form_error("mail_id");
                $this->session->set_flashdata('error_message', '"Error Occured!","' . strip_tags($error_message) . '"');
                redirect(base_url() . 'home');
            } else {
                $res = $this->Dashboard_model->subscribe();
                if ($res) {
                    $this->session->set_flashdata('success_message', '"Success!","Subscribed Successfully!"');
                    redirect(base_url() . 'home');
                } else {
                    $this->session->set_flashdata('warning_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'home');
                }
            }
        }
    }

}
