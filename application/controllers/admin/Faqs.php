<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class Faqs extends MY_Controller {

    private $model = "Faqs_model";

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/' . $this->model);
        $this->data['unique_name'] = 'faqs';
        $this->data['sub_unique_name'] = 'Faqs';
        $this->data['page_title'] = "Faq's";
    }

    function index() {
        $this->data['list'] = $this->{$this->model}->get_data();
        $this->data['form_title'] = 'Add';
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("question", "question", "required|is_unique[faqs.question]", array(
                'required' => 'question cannot be empty',
                "is_unique" => "question must be unique.",
            ));
            $this->form_validation->set_rules("answer", "answer", "required", array(
                'required' => 'answer cannot be empty',
            ));
            $this->form_validation->set_rules("status", "Status", "required", array(
                'required' => 'Status cannot be empty',
            ));
            $this->form_validation->set_rules("faq_page", "faq page", "required", array(
                'required' => 'faq page cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {

            } else {
                $data = array(
                    'question' => $this->input->post('question'),
                    'answer' => $this->input->post('answer'),
                    'status' => $this->input->post('status'),
                    'faq_page' => $this->input->post('faq_page'),
                    'created_at' => time(),
                );
                $res = $this->{$this->model}->add($data);
                if ($res) {
                    $this->session->set_flashdata('success_message', '"Success!","Added Successfully!"');
                    redirect(base_url() . 'admin/faqs');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'admin/faqs');
                }
            }
        }
        $this->admin_view('faqs');
    }

    function update($id) {
        if ((isset($id)) && ($id != '')) {
            $this->data['details'] = $this->{$this->model}->get_details($id);
            if ($this->input->post('submit')) {
                if ($this->data['details']->question == $this->input->post('question')) {
                    $this->form_validation->set_rules("question", "question", "required", array(
                        'required' => 'question cannot be empty',
                    ));
                } else {
                    $this->form_validation->set_rules("question", "question", "required|is_unique[faqs.question]", array(
                        'required' => 'question cannot be empty',
                        "is_unique" => "question must be unique.",
                    ));
                }
                $this->form_validation->set_rules("answer", "answer", "required", array(
                    'required' => 'answer cannot be empty',
                ));
                $this->form_validation->set_rules("status", "Status", "required", array(
                    'required' => 'Status cannot be empty',
                ));
                $this->form_validation->set_rules("faq_page", "faq page", "required", array(
                    'required' => 'faq page cannot be empty',
                ));
                if ($this->form_validation->run() == FALSE) {

                } else {
                    $data = array(
                        'question' => $this->input->post('question'),
                        'answer' => $this->input->post('answer'),
                        'status' => $this->input->post('status'),
                        'faq_page' => $this->input->post('faq_page'),
                        'updated_at' => time(),
                    );
                    $res = $this->{$this->model}->update($id, $data);
                    if ($res) {
                        $this->session->set_flashdata('success_message', '"Success!","Updated Successfully!"');
                        redirect(base_url() . 'admin/faqs');
                    } else {
                        $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                        redirect(base_url() . 'admin/faqs');
                    }
                }
            }
            $this->data['list'] = $this->{$this->model}->get_data();
            $this->data['form_title'] = 'Update';
            $this->admin_view('faqs');
        } else {
            redirect(base_url() . 'admin/faqs');
        }
    }

    function delete($id) {
        if ((isset($id)) && ($id != '')) {
            if ($this->{$this->model}->delete($id)) {
                $this->session->set_flashdata('success_message', "'Deleted Successfully', 'Success'");
                redirect(base_url() . 'admin/faqs');
            } else {
                $this->session->set_flashdata('error_message', "'Please Try Again', 'Error'");
                redirect(base_url() . 'admin/faqs');
            }
        } else {
            redirect(base_url() . 'admin/faqs');
        }
    }

}
