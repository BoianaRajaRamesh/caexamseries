<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class Call_back_request extends MY_Controller {

    private $model = "Contact_model";

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/' . $this->model);
        $this->data['unique_name'] = 'support';
        $this->data['sub_unique_name'] = 'call_back_request';
    }

    function index() {
        $this->data['page_title'] = 'Call Back Request';
        $this->data['list'] = $this->{$this->model}->get_data();
        $this->admin_view('call_back_request');
    }

    function delete($id) {
        if ((isset($id)) && ($id != '')) {
            if ($this->{$this->model}->delete($id)) {
                $this->session->set_flashdata('success_message', "'Deleted Successfully', 'Success'");
                redirect(base_url() . 'admin/call_back_request');
            } else {
                $this->session->set_flashdata('error_message', "'Please Try Again', 'Error'");
                redirect(base_url() . 'admin/call_back_request');
            }
        } else {
            redirect(base_url() . 'admin/call_back_request');
        }
    }

}
