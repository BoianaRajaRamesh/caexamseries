<main>
    <section>
        <div id="main-slider" class="owl-carousel owl-theme">
            <?php foreach ($banners as $row) { ?>
                <div class="item"><img src="<?php echo base_url(); ?><?= $row->image; ?>" srcset="<?php echo base_url(); ?><?= $row->image; ?>, <?php echo base_url(); ?><?= $row->mobile_slider; ?> 768w" alt="" ></div>
            <?php } ?>
        </div>
    </section>
    <section>
        <div class="container-fluid ltyellow">
            <div class="subbox">
                <div class="row">
                    <div class="col-auto">
                        <h4>QUICK <br>PURCHASE</h4>
                    </div>
                    <div class="col">
                        <select class="form-control icon">
                            <option value="">Select Course</option>
                            <?php foreach ($courses_list as $list) { ?>
                                <option value="<?= $list->id; ?>"><?= $list->course; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col">
                        <select class="form-control icon">
                            <option value="">Select Plan</option>
                            <option value="Superior">Superior</option>
                            <option value="Premier">Premier</option>
                            <option value="Diamond">Diamond</option>
                            <option value="Platinum">Platinum</option>
                            <option value="Gold">Gold</option>
                            <option value="Silver">Silver</option>
                        </select>
                    </div>
                    <div class="col">
                        <select class="form-control icon">
                            <option value="">Select Group</option>
                            <option value="">Complete CPT</option>
                            <option value="">1 Subject</option>
                            <option value="">2 Subjects</option>
                            <option value="">3 Subjects</option>
                            <option value="">4 Subjects</option>
                        </select>
                    </div>
                    <div class="col">
                        <select class="form-control icon">
                            <option value="">Select Subject</option>
                        </select>
                    </div>
                </div>
                <div class="row justify-content-center pt-3">
                    <div class="col-lg-7 col-md-8">
                        <div class="row text-center">
                            <div class="col-12 pb-2">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="refplan">
                                    <label class="custom-control-label" for="refplan">I Referred Plans thoroughly and opted</label>
                                </div>
                            </div>
                            <div class="col-12 pb-2">I Have no Idea about about Plans then Select Your Course and <a href="noidea_about_plans" class="text-danger">Click Here</a> </div>
                            <div class="col-12 pt-2">
                                <button type="submit" class="btn btn-grd-red">CONTINUE <i class="fas fa-caret-right"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="row pt-5">
        <div class="col-lg-12">
            <h2 class="section-title">OUR EXAMS</h2>
        </div>
    </div>
    <div class="container pb-5">
        <div class="row justify-content-center">
            <?php
            $i = 1;
            foreach ($courses_list as $row) {
                $class = 'drred';
                if ($i / 3 > 1 && $i / 3 <= 2) {
                    $class = 'ltyellow';
                } else if ($i / 3 >= 2 && $i / 3 <= 3) {
                    $class = 'drorange';
                }
                //echo $i;
                ?>
                <div class="col-lg-4 col-md-4  col-sm-6 col-6"><a href="<?php echo base_url(); ?>courses/view/<?= $row->id; ?>" class="btn_exam <?= $class; ?>"><?= $row->course; ?></a></div>
                <?php
                $i++;
            }
            ?>

        </div>

    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-title">Exclusive Benefits We Offer</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-11 pb-4">
                <img src="<?php echo base_url($home_content->benefits_we_offer_image); ?>" alt="" class="img-fluid">
            </div>
        </div>
        <div class="row pb-5 justify-content-center">
            <div class="col-lg-11">
                <div class="offerbox"><?= $home_content->benefits_we_offer_content; ?></div>
            </div>
        </div>
    </div>
    <div class="features clearfix">
        <div class="row pt-2 position-relative">
            <div class="col-lg-12">
                <h2 class="section-title">PAST STUDENTS STATS</h2>
            </div>
        </div>
        <div class="container-fluid position-relative">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><img src="<?php echo base_url(); ?>/site_assets/images/graduate.png" alt="">
                            <h4><?= $home_content->student_statistics_opted_till_date; ?></h4><span>Students Opted Till Date</span>
                        </li>
                        <li><img src="<?php echo base_url(); ?>/site_assets/images/selection.png" alt="">
                            <h4><?= $home_content->student_statistics_qualified_students; ?></h4><span>Qualified Students</span>
                        </li>
                        <li><img src="<?php echo base_url(); ?>/site_assets/images/5-stars.png" alt="">
                            <h4><?= $home_content->student_statistics_5_star_rated; ?></h4><span>5 Star Rated</span>
                        </li>
                        <li><img src="<?php echo base_url(); ?>/site_assets/images/trophy.png" alt="">
                            <h4><?= $home_content->student_statistics_excemptions_scored; ?></h4><span>Excemptions Scored</span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <section class="pt-5 pb-5">
        <div class="container-fluid">
            <div class="row pt-2">
                <div class="col-lg-12">
                    <h2 class="section-title">HOW IT WORKS ?</h2>
                </div>
            </div>
            <div class="row howorks">
                <?php
                $i = 1;
                foreach ($how_it_works as $row) {
                    ?>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-4"><h4 class="brred">STEP -<?= $i; ?></h4><div class="work-icon"><i class="<?= $row->icon; ?>"></i></div><h4><?= $row->title; ?></h4></div>
                    <?php
                    $i++;
                }
                ?>

            </div>
        </div>
    </section>
    <section class="bg-grey">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-title">WHY CA EXAM SERIES</h2>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 whycontent">
                    <?= $home_content->why_ca_exam_series_content; ?>

                </div>
                <div class="col-lg-6 align-self-center">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= $home_content->why_ca_exam_series_embaded_link; ?>?rel=0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial-bg">
        <div class="row position-relative">
            <div class="col-lg-12">
                <h2 class="section-title text-white">STUDENT TESTIMONIALS</h2>
            </div>
        </div>
        <div class="container position-relative">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <div id="testimonial-slider" class="owl-carousel owl-theme">
                        <?php foreach ($testimonials as $row) { ?>
                            <div class="testimonial">
                                <div class="pic">
                                    <img src="<?php echo base_url($row->image); ?>" alt="">
                                </div>
                                <p class="description"><?= $row->description; ?></p>
                                <h3 class="testimonial-title"><?= $row->name; ?></h3>
                                <span class="post"><?= $row->location; ?></span>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /features -->
    <section class="pt-5 pb-7">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="section-title">OUR STUDENTS FEEDBACK</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="feedback-slider" class="owl-carousel owl-theme">
                        <?php foreach ($feedbacks as $row) { ?>
                            <div class="item">
                                <div class="box">
                                    <img src="http://i3.ytimg.com/vi/<?= $row->link; ?>/hqdefault.jpg" alt="">

                                    <ul class="icon">
                                        <li><a data-fancybox href="https://www.youtube.com/watch?v=<?= $row->link; ?>&feature=youtu.be"><i class="fal fa-play"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-5 pb-7">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="section-title">SAMPLE CHECKED PAPERS</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="sampleq-slider" class="owl-carousel owl-theme">
                        <?php for ($i = 1; $i <= 10; $i++) { ?>
                            <div class="item">
                                <img src="<?php echo base_url(); ?>site_assets/images/sampletest.jpg" alt="" class="img-thumbnail">
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-2 pb-7">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="section-title">FAQ's</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row accordion" id="accordionExample">
                <div class="col-lg-12 faqcontentbox">
                    <div class="row">
                        <?php foreach ($faqs as $row) { ?>
                            <div class="card col-lg-6">
                                <div class="card-header" id="heading<?= $row->id; ?>">
                                    <h2 class="mb-0">
                                        <button class="btn btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse<?= $row->id; ?>" aria-expanded="false" aria-controls="collapse<?= $row->id; ?>">
                                            <?= $row->question; ?>
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapse<?= $row->id; ?>" class="collapse" aria-labelledby="heading<?= $row->id; ?>" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <?= $row->answer; ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>
</main>
<!-- /main -->
<?php
$this->load->view('website/includes/footer');
?>