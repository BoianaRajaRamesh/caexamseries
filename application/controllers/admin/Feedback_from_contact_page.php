<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback_from_contact_page extends MY_Controller {

    private $model = "Contact_model";

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/' . $this->model);
        $this->data['unique_name'] = 'support';
        $this->data['sub_unique_name'] = 'feedback_from_contact_page';
    }

    function index() {
        $this->data['page_title'] = 'Feedback';
        $this->data['list'] = $this->{$this->model}->get_feedback_data();
        $this->admin_view('feedback_from_contact_page');
    }

    function delete($id) {
        if ((isset($id)) && ($id != '')) {
            if ($this->{$this->model}->delete_feedback($id)) {
                $this->session->set_flashdata('success_message', "'Deleted Successfully', 'Success'");
                redirect(base_url() . 'admin/feedback_from_contact_page');
            } else {
                $this->session->set_flashdata('error_message', "'Please Try Again', 'Error'");
                redirect(base_url() . 'admin/feedback_from_contact_page');
            }
        } else {
            redirect(base_url() . 'admin/feedback_from_contact_page');
        }
    }

}
