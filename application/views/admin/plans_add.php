<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>admin/dashboard">Dashboard</a>
            </li>
            <li>
                <a href="<?= base_url(); ?>admin/plans">Plans</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div>
            <div class="ibox float-e-margins">
                <div class="ibox-content">
                    <form method="post" class="form-horizontal" action="" enctype="multipart/form-data" autocomplete="off">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Title</label>
                                <div class="col-sm-12">
                                    <input type="text" name="plan_title" class="form-control" required="" value="<?php if (isset($details)) echo $details->plan_title ?>" placeholder="Title">
                                </div>
                                <?php echo form_error('plan_title', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Actual Price</label>
                                <div class="col-sm-12">
                                    <input type="text" name="actual_price" class="form-control number" required="" value="<?php if (isset($details)) echo $details->actual_price ?>" placeholder="actual price">
                                </div>
                                <?php echo form_error('actual_price', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Discount Price</label>
                                <div class="col-sm-12">
                                    <input type="text" name="discount_price" class="form-control number" required="" value="<?php if (isset($details)) echo $details->discount_price ?>" placeholder="discount price">
                                </div>
                                <?php echo form_error('discount_price', '<div class="error">', '</div>'); ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Courses</label>
                                <div class="col-sm-12">
                                    <select class="form-control" required="" name="course_id">
                                        <option value="">Select Course</option>
                                        <?php
                                        foreach ($courses_list as $row) {
                                            $selected = ((isset($details->course_id) && $details->course_id == $row->id)) ? 'selected' : '';
                                            ?>
                                            <option value="<?= $row->id; ?>" <?= $selected; ?>><?= $row->course; ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php echo form_error('course_id', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Exams</label>
                                <div class="col-sm-12">
                                    <!--required=""-->
                                    <select class="form-control"  name="exam_id">
                                        <option value="">Select Exam</option>
                                        <?php
                                        foreach ($exams as $row) {
                                            $selected = ((isset($details->exam_id) && $details->exam_id == $row->id)) ? 'selected' : '';
                                            ?>
                                            <option value="<?= $row->id; ?>" <?= $selected; ?>><?= $row->title; ?></option>
                                        <?php } ?>
                                    </select>
                                    <?php echo form_error('exam_id', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Exam Mode</label>
                                <div class="col-sm-12">
                                    <select class="form-control" name="exam_mode" required="">
                                        <option value="">Select Exam Mode</option>
                                        <option value="Schedule" <?= (isset($details->exam_mode) && $details->exam_mode == 'Schedule') ? 'selected' : ''; ?>>Schedule</option>
                                        <option value="Un-Schedule" <?= (isset($details->exam_mode) && $details->exam_mode == 'Un-Schedule') ? 'selected' : ''; ?>>Un-Schedule</option>
                                        <option value="PassGuarantee" <?= (isset($details->exam_mode) && $details->exam_mode == 'PassGuarantee') ? 'selected' : ''; ?>>PassGuarantee</option>
                                        <option value="MockExams" <?= (isset($details->exam_mode) && $details->exam_mode == 'MockExams') ? 'selected' : ''; ?>>MockExams</option>
                                    </select>
                                    <?php echo form_error('exam_mode', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Schedule PDF</label>
                                <div class="col-sm-12">
                                    <input type="file" id="spdf" <?php if (!isset($details->schedule_pdf)) { ?> required="" <?php } ?> accept="application/pdf" onchange="check_file_size_pdf(this.id)" name="schedule_pdf" class="form-control">
                                    <?php if (isset($details)) { ?>
                                        <span>Only PDF formats are available <b><a target="_blank" href="<?= base_url($details->schedule_pdf); ?>">Previous PDF</a></b></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-12 ">Colors</label>
                                <div class="col-sm-12">
                                    <select class="form-control" required="" name="color">
                                        <option value="">Select color</option>
                                        <option value="blue" <?= (isset($details->color) && $details->color == 'blue') ? 'selected' : ''; ?>>Blue</option>
                                        <option value="green" <?= (isset($details->color) && $details->color == 'green') ? 'selected' : ''; ?>>Green</option>
                                        <option value="orange" <?= (isset($details->color) && $details->color == 'orange') ? 'selected' : ''; ?>>Orange</option>
                                    </select>
                                    <?php echo form_error('color', '<div class="error">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6">
                            <label class="control-label">Subject Wise</label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-success">
                                        <input name="subject_wise" id="radio11" value="yes" type="radio" <?= (isset($details->subject_wise) && ($details->subject_wise == 'yes')) ? 'checked' : '' ?> required>
                                        <label for="radio11">Yes</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-danger">
                                        <input name="subject_wise" id="radio21" value="no" type="radio" <?= (isset($details->subject_wise) && ($details->subject_wise == 'no')) ? 'checked' : '' ?> required>
                                        <label for="radio21">No</label>
                                    </div>
                                </label>
                            </div>
                            <?php echo form_error('subject_wise', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="col-md-6">
                            <label class="control-label">Status</label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-success">
                                        <input name="status" id="radio1" value="active" type="radio" <?= (isset($details->status) && ($details->status == 'active')) ? 'checked' : '' ?> required>
                                        <label for="radio1">Active</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-danger">
                                        <input name="status" id="radio2" value="inactive" type="radio" <?= (isset($details->status) && ($details->status == 'inactive')) ? 'checked' : '' ?> required>
                                        <label for="radio2">Inactive</label>
                                    </div>
                                </label>
                            </div>
                            <?php echo form_error('status', '<div class="error">', '</div>'); ?>
                        </div>


                        <div class="col-md-12">
                            <label class="control-label">Description Points</label>
                            <?php
                            if (isset($details->plan_wise_points)) {
                                foreach ($details->plan_wise_points as $item) {
                                    ?>
                                    <div id="old_more_points<?= $item->id; ?>">
                                        <div class="col-md-5">
                                            <div class="form-group"><input type="text" name="points[]" class="form-control" required="" value="<?= $item->description; ?>" placeholder="Points"></div>
                                        </div>
                                        <div class="col-md-1">
                                            <div class="form-group"><button type="button" class="btn btn-danger" onclick="remove_more_points('old_more_points<?= $item->id; ?>')"><i class="fa fa-trash"></i></button></div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                            <div id="desc_points">
                                <div>
                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <input type="text" name="points[]" class="form-control" <?php if (!isset($details->plan_wise_points)) { ?> required="" <?php } ?> value="" placeholder="Points">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <button type="button" onclick="add_more_points()" class="btn btn-primary"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_error('points[0]', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="col-md-12">
                            <label class="control-label">Subjects List</label><br>
                            <?php
                            foreach ($subjects as $row) {
                                $selected = '';
                                if (isset($details->plan_wise_subjects)) {
                                    foreach ($details->plan_wise_subjects as $item) {
                                        if ($item->subject_id == $row->id) {
                                            $selected = 'checked';
                                        }
                                    }
                                }
                                ?>
                                <label class="checkbox-inline" style="margin-right: 30px;">
                                    <input type="checkbox" <?= $selected; ?> value="<?= $row->id; ?>" name="subjects_list[]">
                                    <?= $row->subject; ?>
                                </label>
                            <?php } ?>
                            <?php echo form_error('subjects_list[0]', '<div class="error">', '</div>'); ?>
                        </div>
                        <div class="col-md-2">
                            <label class="col-sm-12"> &nbsp; </label>
                            <input type="submit" name="submit" class="btn btn-primary btn-block" value="<?= $page_title; ?>">
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('admin/includes/footer'); ?>
<script type="text/javascript">
    CKEDITOR.replace("description");
</script>

<script>
    k = 1;
    function add_more_points() {
        var html = '<div id="more_points' + k + '"><div class="col-md-5"><div class="form-group"><input type="text" name="points[]" class="form-control" required="" value="" placeholder="Points"></div></div><div class="col-md-1"><div class="form-group"><button type="button" class="btn btn-danger" onclick=remove_more_points("more_points' + k + '")><i class="fa fa-trash"></i></button></div></div></div>';
        $("#desc_points").append(html);
        k++;
    }
    function remove_more_points(remove_id) {
        $("#" + remove_id).remove();
    }
    function check_file_size_pdf(val) {
        var uploadField = document.getElementById(val);
        var FileName = uploadField.files[0].name;
        var FileExtension = FileName.split('.')[FileName.split('.').length - 1]; //alert(FileExtension);
        if (!(FileExtension == 'pdf' || FileExtension == 'PDF')) {
            swal({
                title: "Warning!",
                text: "Please Upload pdf Files Only",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                closeOnConfirm: false
            });
            document.getElementById(val).value = "";
        } else if (uploadField.files[0].size > 5000000) {
            swal({
                title: "Warning!",
                text: "Max upload size 5 MB",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "OK",
                closeOnConfirm: false
            });
            document.getElementById(val).value = "";
        }
    }

</script>