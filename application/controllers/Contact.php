<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MY_Controller {

    private $model = "Contact_model";

    function __construct() {
        parent::__construct();
        $this->load->model("admin/Course_model");
        $this->load->model("admin/" . $this->model);
    }

    public function index() {
        $this->data['page'] = 'Contact';
        $this->web_view('contact', $this->data);
    }

    function call_back_request() {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("name", "name", "required", array(
                'required' => 'name cannot be empty',
            ));
            $this->form_validation->set_rules("email", "email", "required", array(
                'required' => 'email cannot be empty',
            ));
            $this->form_validation->set_rules("phone_number", "phone_number", "required", array(
                'required' => 'phone number cannot be empty',
            ));
            $this->form_validation->set_rules("course", "course", "required", array(
                'required' => 'course cannot be empty',
            ));
            $this->form_validation->set_rules("reason", "reason", "required", array(
                'required' => 'reason cannot be empty',
            ));
            $this->form_validation->set_rules("feedback", "feedback", "required", array(
                'required' => 'feedback cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {
                $this->data['page'] = 'Contact';
                $this->web_view('contact', $this->data);
            } else {
                $data = array(
                    'name' => $this->input->post('name'),
                    'email' => $this->input->post('email'),
                    'phone_number' => $this->input->post('phone_number'),
                    'course' => $this->input->post('course'),
                    'reason' => $this->input->post('reason'),
                    'feedback' => $this->input->post('feedback'),
                    'created_at' => time(),
                    'status' => 'active'
                );
                $res = $this->{$this->model}->add($data);
                if ($res) {
                    $this->session->set_flashdata('success_message', '"Success!","Request Sent Successfully!"');
                    redirect(base_url() . 'contact');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'contact');
                }
            }
        }
    }

    function feedback() {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("fname", "name", "required", array(
                'required' => 'name cannot be empty',
            ));
            $this->form_validation->set_rules("femail", "email", "required", array(
                'required' => 'email cannot be empty',
            ));
            $this->form_validation->set_rules("fphone_number", "phone_number", "required", array(
                'required' => 'phone number cannot be empty',
            ));
            $this->form_validation->set_rules("fcourse", "course", "required", array(
                'required' => 'course cannot be empty',
            ));
            $this->form_validation->set_rules("ffeedback", "feedback", "required", array(
                'required' => 'feedback cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {
                $this->data['page'] = 'Contact';
                $this->web_view('contact', $this->data);
            } else {
                $data = array(
                    'name' => $this->input->post('fname'),
                    'email' => $this->input->post('femail'),
                    'phone_number' => $this->input->post('fphone_number'),
                    'course' => $this->input->post('fcourse'),
                    'feedback' => $this->input->post('ffeedback'),
                    'created_at' => time(),
                    'status' => 'active'
                );
                $res = $this->{$this->model}->add_feedback($data);
                if ($res) {
                    $this->session->set_flashdata('success_message', '"Success!","Feedback Sent Successfully!"');
                    redirect(base_url() . 'contact');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'contact');
                }
            }
        }
    }

}
