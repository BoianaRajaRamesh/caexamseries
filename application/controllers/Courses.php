<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Courses extends MY_Controller {

    private $model = "Courses_model";

    function __construct() {
        parent::__construct();
        $this->load->model($this->model);
        $this->load->model('Dashboard_model');
        $this->load->model('admin/Home_page_content_model');
        $this->load->model('admin/How_it_works_model');
        $this->load->model('admin/Feedback_model');
        $this->load->model('admin/Testimonial_model');
        $this->load->model('admin/Faqs_model');
        $this->load->model('admin/Banners_model');
    }

    public function view($id) {
        $this->data['course_id'] = $id;
        $this->data['banners'] = $this->{$this->model}->get_active_banners($id);
        $this->data['faqs'] = $this->Faqs_model->get_all_active_data();
        $this->data['feedbacks'] = $this->Feedback_model->get_active_data();
        $this->data['testimonials'] = $this->Testimonial_model->get_active_data();
        $this->data['home_content'] = $this->Home_page_content_model->get_details(1);
        $this->data['how_it_works'] = $this->How_it_works_model->get_active_data();
        $this->data['exams'] = $this->{$this->model}->exams_list();
        foreach ($this->data['exams'] as $row) {
            $row->plans = $this->{$this->model}->get_plans_list($row->id, $id);
        }
        $this->data['page'] = 'Course';
        $this->web_view('course_view', $this->data);
    }

}
