<main>
    <section>
        <div id="main-slider" class="owl-carousel owl-theme">
            <?php foreach ($banners as $row) { ?>
                <div class="item"><img src="<?php echo base_url($row->image); ?>" alt=""></div>
            <?php } ?>
<!--            <div class="item"><img src="<?php echo base_url(); ?>/site_assets/images/inner_slider2.jpg" alt=""></div>
<div class="item"><img src="<?php echo base_url(); ?>/site_assets/images/inner_slider3.jpg" alt=""></div>
<div class="item"><img src="<?php echo base_url(); ?>/site_assets/images/inner_slider4.jpg" alt=""></div>-->
        </div>
    </section>

    <div class="clearfix">&nbsp;</div>

    <div class="container-fluid pt-3">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-title">Exclusive Benefits We Offer</h2>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-lg-11 pb-4">
                <img src="<?php echo base_url($home_content->benefits_we_offer_image); ?>" alt="" class="img-fluid">
            </div>
        </div>
        <div class="row pb-5 justify-content-center">
            <div class="col-lg-11">
                <div class="offerbox"><?= $home_content->benefits_we_offer_content; ?></div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-title">We Offer <?= count($exams); ?> Types of Exams</h2>
            </div>
        </div>

        <div class="row testsexams">
            <?php foreach ($exams as $row) { ?>
                <div class="col-lg-4 col-md-6 col-sm-12 align-self-center">
                    <div class="csubox-<?= $row->color; ?>">
                        <h3><?= $row->title; ?></h3>
                        <p><?= $row->description; ?></p>
                        <a href="courses/view/<?= $course_id; ?>#<?= str_replace(' ', '', $row->title); ?>" class="btn_csu"><?= $row->title; ?></a>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-lg-12 text-center"><p class="pt-3 pb-3"><strong>Exams will be conducted in Scheduled Mode & Unschedule Mode </strong><a href="noidea_about_plans" class="btn btn-sm btn-danger"><i class="fa fa-info-circle"></i> Click here</a></p></div>
        </div>

    </div>


    <div class="exam-btns">
        <div class="extype">Select Your Exam Type :</div>
        <div class="container">
            <div class="row">
                <?php foreach ($exams as $row) { ?>
                    <div class="col-md-4">
                        <a class="Mybutton" href="courses/view/<?= $course_id; ?>#<?= str_replace(' ', '', $row->title); ?>">
                            <div class="head_table">
                                <h3><?= $row->title; ?></h3>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php foreach ($exams as $row) { ?>
        <div class="container-fluid" id="<?= str_replace(' ', '', $row->title); ?>">
            <div class="row justify-content-center">
                <div class="col-lg-11 col-md-10 col-12">

                    <div class="new-plans" id="Silver">
                        <h3 class="ribbon plan-hdd">
                            <span class="ribbon-content"><?= $row->title; ?></span></h3>
                        <h3 class="ribbontwo headingsubtitle">
                            <span class="ribbon-content"><?= $row->description; ?></span></h3>
                        <div class="row justify-content-center">
                            <div class="col-lg-10 col-md-12">
                                <div class="row justify-content-center">

                                    <div class="col-lg-3 col-md-4 text-right text-sm-center  align-self-center"><h5>Select Your Exam Mode <i class="fal fa-arrow-right"></i></h5> </div>
                                    <div class="col-lg-3 col-md-4">
                                        <ul class="nav nav-pills justify-content-center" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link active" href="#<?= str_replace(' ', '', $row->title); ?>_schedule" role="tab" data-toggle="tab">
                                                    Schedule
                                                </a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link" href="#<?= str_replace(' ', '', $row->title); ?>_unschedule" role="tab" data-toggle="tab">
                                                    Un-Schedule
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-3 col-md-4 text-left text-sm-center align-self-center pt-sm-2 d-none d-lg-block d-md-block"><h5><i class="fal fa-arrow-left"></i> Select Your Exam Mode </h5> </div>

                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="tab-content tab-space">
                        <div class="tab-pane active" id="<?= str_replace(' ', '', $row->title); ?>_schedule">
                            <br>
                            <div class="row">
                                <?php
                                foreach ($row->plans as $item) {
                                    if ($item->exam_mode == 'Schedule') {
                                        if ($item->subject_wise == 'no') {
                                            ?>
                                            <div class="col-lg-3 col-md-6 col-6">
                                                <div class="pricingTable <?= $item->color; ?>">
                                                    <div class="pricingTable-header">
                                                        <h3 class="title"><?= $item->plan_title; ?></h3>
                                                    </div>
                                                    <div class="price-value">
                                                        <span class="amount"><i class="fal fa-rupee-sign"></i> <span id="amount_<?= $item->id; ?>"><?= $item->discount_price; ?></span>/-</span>
                                                        <small><i class="fal fa-rupee-sign"></i> <?= $item->actual_price; ?>/-</small>
                                                    </div>
                                                    <ul class="pricing-content">
                                                        <?php foreach ($item->points as $points) { ?>
                                                            <li><?= $points->description; ?></li>
                                                        <?php } ?>
                                                    </ul>
                                                    <div class="pricingTable-signup">
                                                        <a href="#" data-toggle="modal" data-target="#add_cart_pop"><i class="fal fa-arrow-circle-right"></i> Buy Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-lg-3 col-md-6 col-6">
                                                <div class="pricingTable <?= $item->color; ?>">
                                                    <div class="pricingTable-header">
                                                        <h3 class="title"><?= $item->plan_title; ?></h3>
                                                    </div>
                                                    <div class="price-value">
                                                        <span class="amount"><i class="fal fa-rupee-sign"></i> <span id="amount_<?= $item->id; ?>"><?= $item->actual_price; ?></span>/-</span>
                                                    </div>
                                                    <select class="form-control myselect mx-3" multiple placeholder="Select Subjects">
                                                        <?php foreach ($item->subjects as $points) { ?>
                                                            <option value="<?= $points->subject_id . ',' . $points->amount; ?>,amount_<?= $item->id; ?>"><?= $points->subject; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <div class="row">
                                                        <div class="col-md-2 col-2 offset-md-1">
                                                            <a href="#individualModal" data-toggle="modal"><i class="fas fa-info-circle fa-4x text-danger"></i></a>
                                                        </div>
                                                        <div class="col-md-9 col-9">
                                                            <p class="pt-1 pl-1 text-left">Click Here for <br>Individual subjects info.</p>
                                                        </div>
                                                    </div>
                                                    <div class="pricingTable-signup">
                                                        <a href="#"><i class="fal fa-arrow-circle-right"></i> Buy Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function () {
                                    $('.myselect').each(function () {
                                        var $myselect = $(this);
                                        $myselect.SumoSelect({
                                            placeholder: $myselect.data("text")
                                        });
                                    });
                                });
                            </script>

                        </div>


                        <div class="tab-pane" id="<?= str_replace(' ', '', $row->title); ?>_unschedule"> <br>

                            <div class="row">

                                <?php
                                foreach ($row->plans as $item) {
                                    if ($item->exam_mode == 'Un-Schedule') {
                                        if ($item->subject_wise == 'no') {
                                            ?>
                                            <div class="col-lg-3 col-md-6 col-6">
                                                <div class="pricingTable <?= $item->color; ?>">
                                                    <div class="pricingTable-header">
                                                        <h3 class="title"><?= $item->plan_title; ?></h3>
                                                    </div>
                                                    <div class="price-value">
                                                        <span class="amount"><i class="fal fa-rupee-sign"></i> <?= $item->discount_price; ?>/-</span>
                                                        <small><i class="fal fa-rupee-sign"></i> <?= $item->actual_price; ?>/-</small>
                                                    </div>
                                                    <ul class="pricing-content">
                                                        <?php foreach ($item->points as $points) { ?>
                                                            <li><?= $points->description; ?></li>
                                                        <?php } ?>
                                                    </ul>
                                                    <div class="pricingTable-signup">
                                                        <a href="#" data-toggle="modal" data-target="#add_cart_pop"><i class="fal fa-arrow-circle-right"></i> Buy Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-lg-3 col-md-6 col-6">
                                                <div class="pricingTable <?= $item->color; ?>">
                                                    <div class="pricingTable-header">
                                                        <h3 class="title"><?= $item->plan_title; ?></h3>
                                                    </div>
                                                    <div class="price-value">
                                                        <span class="amount"><i class="fal fa-rupee-sign"></i> <span id="amount_<?= $item->id; ?>"><?= $item->actual_price; ?></span>/-</span>
                                                    </div>
                                                    <select class="form-control myselect mx-3" multiple placeholder="Select Subjects">
                                                        <?php foreach ($item->subjects as $points) { ?>
                                                            <option value="<?= $points->subject_id . ',' . $points->amount; ?>,amount_<?= $item->id; ?>"><?= $points->subject; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                    <div class="row">
                                                        <div class="col-md-2 col-2 offset-md-1">
                                                            <a href="#individualModal" data-toggle="modal"><i class="fas fa-info-circle fa-4x text-danger"></i></a>
                                                        </div>
                                                        <div class="col-md-9 col-9">
                                                            <p class="pt-1 pl-1 text-left">Click Here for <br>Individual subjects info.</p>
                                                        </div>
                                                    </div>
                                                    <div class="pricingTable-signup">
                                                        <a href="#"><i class="fal fa-arrow-circle-right"></i> Buy Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php
                                    }
                                }
                                ?>
                            </div></div>
                    </div>

                </div>
            </div>
        </div>
    <?php } ?>
    <section class="pt-5 pb-5">
        <div class="container-fluid">
            <div class="row pt-2">
                <div class="col-lg-12">
                    <h2 class="section-title">HOW IT WORKS ?</h2>
                </div>
            </div>
            <div class="row howorks">
                <?php
                $i = 1;
                foreach ($how_it_works as $row) {
                    ?>
                    <div class="col-lg-2 col-md-4 col-sm-4 col-4"><h4 class="brred">STEP -<?= $i; ?></h4><div class="work-icon"><i class="<?= $row->icon; ?>"></i></div><h4><?= $row->title; ?></h4></div>
                    <?php
                    $i++;
                }
                ?>
            </div>
        </div>
    </section>
    <section class="bg-grey">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="section-title">WHY CA EXAM SERIES</h2>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 whycontent">
                    <?= $home_content->why_ca_exam_series_content; ?>
<!--                    <h3><span>CA Exam Series</span> Is an Unique & Excelled Platform made for ca students to Clear their Exams.</h3>
                    <ul>
                        <li>Systematic Exam Schedules</li>
                        <li>Balanced Question Papers</li>
                        <li>Legitimate Evaluation</li>
                        <li>Expertise Suggested Answers</li>
                    </ul>
                    <p>Which Gives Tremendous Practice Effective Time Management, Superlative Presentation Skills, Upsurge Confidence Levels.</p>-->
                </div>
                <div class="col-lg-6 align-self-center">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?= $home_content->why_ca_exam_series_embaded_link; ?>?rel=0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="testimonial-bg mb-5">
        <div class="row position-relative">
            <div class="col-lg-12">
                <h2 class="section-title text-white">WHAT OUR STUDENTS SAYS ABOUT US</h2>
            </div>
        </div>
        <div class="container position-relative">
            <div class="row justify-content-center">
                <div class="col-lg-9">
                    <div id="testimonial-slider" class="owl-carousel owl-theme">
                        <?php foreach ($testimonials as $row) { ?>
                            <div class="testimonial">
                                <div class="pic">
                                    <img src="<?php echo base_url($row->image); ?>" alt="">
                                </div>
                                <p class="description"><?= $row->description; ?></p>
                                <h3 class="testimonial-title"><?= $row->name; ?></h3>
                                <span class="post"><?= $row->location; ?></span>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-5 pb-7">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="section-title">OUR STUDENTS FEEDBACK</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="feedback-slider" class="owl-carousel owl-theme">
                        <?php foreach ($feedbacks as $row) { ?>
                            <div class="item">
                                <div class="box">
                                    <img src="http://i3.ytimg.com/vi/<?= $row->link; ?>/hqdefault.jpg" alt="">

                                    <ul class="icon">
                                        <li><a data-fancybox href="https://www.youtube.com/watch?v=<?= $row->link; ?>&feature=youtu.be"><i class="fal fa-play"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pt-5 pb-7">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="section-title">SAMPLE CHECKED PAPERS</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="sampleq-slider" class="owl-carousel owl-theme">
                        <?php for ($i = 1; $i <= 10; $i++) { ?>
                            <div class="item">
                                <img src="<?php echo base_url(); ?>site_assets/images/sampletest.jpg" alt="" class="img-thumbnail">
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pb-7">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="section-title">FAQ's</h2>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row accordion" id="accordionExample">
                <div class="col-lg-12 faqcontentbox">
                    <div class="row">
                        <?php foreach ($faqs as $row) { ?>
                            <div class="card col-lg-6">
                                <div class="card-header" id="heading<?= $row->id; ?>">
                                    <h2 class="mb-0">
                                        <button class="btn btn-block text-left" type="button" data-toggle="collapse" data-target="#collapse<?= $row->id; ?>" aria-expanded="false" aria-controls="collapse<?= $row->id; ?>">
                                            <?= $row->question; ?>
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapse<?= $row->id; ?>" class="collapse" aria-labelledby="heading<?= $row->id; ?>" data-parent="#accordionExample">
                                    <div class="card-body">
                                        <?= $row->answer; ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                    </div>
                </div>
                <div class="clearfix"></div>

            </div>
        </div>
    </section>
</main>

<!-- /main -->

<?php
$this->load->view('website/includes/footer');
?>
<script>
    amount_display_history = '';
    $(document).ready(function () {
        $('.myselect').on('change', function () {
            var obj = [];
            items = '';
            total = 0;
            amount = [];
            amount_display_id = '';
            if (amount_display_history != '') {
                $('#' + amount_display_history).text(total);
            }
            $(this).find('option:selected').each(function (i) {
                obj.push($(this).val());
                amount.push($(this).val().toString().split(",")[1]);
                amount_display_id = $(this).val().toString().split(",")[2];
            });
            // console.log(amount);
            for (var i = 0; i < amount.length; i++) {
                total += parseInt(amount[i]);
            }
            $('#' + amount_display_id).text(total);
            amount_display_history = amount_display_id;
//            alert(total);
        });
    });

</script>
