<main>
    <div class="container padd-50">
        <div class="row">
            <div class="col-lg-8 col-md-12 blogcont">
                <div class="row">
                    <?php foreach ($blogs_list as $list) { ?>
                        <div class="col-md-6">
                            <div class="box_grid wow">
                                <figure class="block-reveal">
                                    <a href="<?php echo base_url(); ?>blog_view"><img src="<?php echo base_url($list->image); ?>" class="img-fluid" alt="" style="height:180px;"></a>
                                </figure>
                                <div class="wrapper">
                                    <span class="text-success"><i class="fal fa-user"></i> Admin</span>
                                    <p><?= $list->title; ?></p><br><hr>
                                    <a href="<?php echo base_url(); ?>blog/blog_view/<?= $list->id; ?>" class="btn btn-warning float-right">View Details</a>
                                </div>

                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
            <div class="col-lg-4 col-md-12">
                <div class="card text-white mb-3">
                    <div class="card-header bg-warning m-0 p-0 pt-1 pl-2"><h4>Follow Us</h4></div>
                    <div class="card-body blogpost">
                        <div class="follow_us">
                            <ul clas="pb-0 mb-0" style="margin-bottom:0px;">
                                <li><a href="<?php echo $site_settings->facebook; ?>" target="_blank"><i class="ti-facebook"></i></a></li>
                                <li><a href="<?php echo $site_settings->twitter; ?>" target="_blank"><i class="ti-twitter-alt"></i></a></li>
                                <li><a href="<?php echo $site_settings->linked_in; ?>" target="_blank"><i class="ti-instagram"></i></a></li>
                                <li><a href="<?php echo $site_settings->youtube; ?>" target="_blank"><i class="ti-dribbble"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card text-white mb-3">
                    <div class="card-header bg-warning m-0 p-0 pt-1 pl-2"><h4>Popular Posts</h4></div>
                    <div class="card-body">
                        <?php foreach ($popular_blogs_list as $row) { ?>
                            <div class="row">
                                <div class="col-5"><img src="<?php echo base_url($row->image); ?>" class="img-fluid img-thumbnail" alt=""></div>
                                <div class="col-7 postpop">
                                    <h5 class="mt-0"><a href="<?php echo base_url(); ?>blog/blog_view/<?= $row->id; ?>"><?= $row->title; ?></a></h5>
                                </div>
                            </div> <hr>
                        <?php } ?>
                    </div>
                </div>
                <div class="card text-white mb-3">
                    <div class="card-header bg-warning m-0 p-0 pt-1 pl-2"><h4>Categories</h4></div>
                    <div class="card-body">
                        <ul class="blogcatlist">
                            <?php foreach ($courses_list as $list) { ?>
                                <li><a href="#"><?= $list->course; ?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>



    </div>
</main>


<!-- /main -->

<?php
$this->load->view('website/includes/footer');
?>