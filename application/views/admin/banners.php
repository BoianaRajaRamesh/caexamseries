<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2><?= $page_title; ?></h2>
        <ol class="breadcrumb">
            <li>
                <a href="<?= base_url(); ?>admin/dashboard">Dashboard</a>
            </li>
            <li class="active">
                <strong><?= $page_title; ?></strong>
            </li>
        </ol>
    </div>
    <div class="col-lg-2">
    </div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-md-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <!-- table-responsive -->
                    <div class="">
                        <table id="myTable" class="table table-striped">
                            <thead>
                                <tr>
                                    <!--<th>S.No</th>-->
                                    <th>Priority</th>
                                    <th>Web Banner</th>
                                    <th>Mobile Banner</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (isset($banners) && (count($banners) > 0)) {
                                    $i = 1;
                                    foreach ($banners as $item) {
                                        ?>
                                        <tr>
                                            <!--<td><?= $i ?></td>-->
                                            <td><?= $item->priority ?></td>
                                            <td><img src="<?= base_url() . $item->image ?>" class="img-responsive" style="max-width: 150px; max-height: 100px"></td>
                                            <td><img src="<?= base_url() . $item->mobile_slider ?>" class="img-responsive" style="max-width: 150px; max-height: 100px"></td>
                                            <td><?php
                                                if ($item->status == 1) {
                                                    echo "Active";
                                                } else {
                                                    echo "InActive";
                                                }
                                                ?></td>

                                            <td>
                                                <a href="<?= base_url() ?>admin/banners/update/<?= $item->id ?>" class="btn btn-sm btn-circle btn-success"><i class="fa fa-edit"></i></a>
                                                <button type="button" class="btn btn-sm btn-circle btn-danger delete_item" value="<?= $item->id ?>"><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5><?= $form_title; ?> <?= $page_title; ?></h5>
                </div>
                <div class="ibox-content">
                    <form action="" method="post" enctype="multipart/form-data">

                        <div class="form-group">
                            <label>Web Banner SIZE : 1300 X 1200</label>
                            <input class="form-control" type="file" name="image" <?= (isset($banners_details->id)) ? '' : 'required' ?>>
                        </div>
                        <?php
                        if (isset($banners_details->image) && $banners_details->image != '') {
                            ?>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <input type="hidden" name="previous_image" value="<?= $banners_details->image ?>"/>
                                    <img src="<?= base_url() . $banners_details->image ?>" class="img-responsive"><br>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="form-group">
                            <label>Mobile Banner SIZE : 1000 X 800</label>
                            <input class="form-control" type="file" name="mobile_slider" <?= (isset($banners_details->id)) ? '' : 'required' ?>>
                        </div>
                        <?php
                        if (isset($banners_details->mobile_slider) && $banners_details->mobile_slider != '') {
                            ?>
                            <div class="form-group">
                                <div class="col-sm-offset-3 col-sm-6">
                                    <input type="hidden" name="previous_image1" value="<?= $banners_details->mobile_slider ?>"/>
                                    <img src="<?= base_url() . $banners_details->mobile_slider ?>" class="img-responsive"><br>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="form-group">
                            <label>Priority</label>
                            <input class="form-control number" type="text" name="priority" placeholder="Priority" value="<?= (isset($banners_details->id)) ? $banners_details->priority : '' ?>" required="">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Status</label>
                            <div class="radio-list">
                                <label class="radio-inline p-0">
                                    <div class="radio radio-success">
                                        <input name="status" id="radio1" value="1" type="radio" <?= (isset($banners_details->status) && ($banners_details->status == 1)) ? 'checked' : '' ?> required>
                                        <label for="radio1">Active</label>
                                    </div>
                                </label>
                                <label class="radio-inline">
                                    <div class="radio radio-danger">
                                        <input name="status" id="radio2" value="0" type="radio" <?= (isset($banners_details->status) && ($banners_details->status == 0)) ? 'checked' : '' ?> required>
                                        <label for="radio2">Inactive</label>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-rounded px-20 btn-info waves-effect waves-light m-r-10" name="submit" value="submit">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$this->load->view('admin/includes/footer');
?>
<script type="text/javascript">
    $('#text').keypress(function (e) {
        var regex = new RegExp("^[a-zA-Z0-9 ]+$");
        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
        if (regex.test(str)) {
            return true;
        }

        e.preventDefault();
        return false;
    });
    $(document).ready(function () {
        $('#myTable').DataTable();
        //Delete Confirmation Script starts here
        $(document).on("click", '.delete_item', function () {
            var del_id = $(this).val();
            swal({
                title: "Are you sure?",
                text: "You want to Delete entire data related to this!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function () {
                window.location = "<?= base_url() ?>admin/banners/delete/" + del_id;
            });
        });
        //Delete Confirmation Script ends here
    });
</script>