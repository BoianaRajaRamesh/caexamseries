<?php

/* ==================== admin controller =================== */
defined('BASEPATH') OR exit('No direct script access allowed');

class Plans extends MY_Controller {

    private $model = "Plans_model";

    function __construct() {
        parent::__construct();
        $this->login_required();
        $this->load->model('admin/' . $this->model);
        $this->load->model('admin/Subject_model');
        $this->data['unique_name'] = 'plans';
        $this->data['sub_unique_name'] = 'plans';
    }

    function index() {
        $this->data['page_title'] = 'Plans';
        $this->data['list'] = $this->{$this->model}->get_data();
        $this->admin_view('plans');
    }

    function add() {
        if ($this->input->post('submit')) {
            $this->form_validation->set_rules("plan_title", "Plan Title", "required", array(
                'required' => 'Plan Title cannot be empty',
            ));
            $this->form_validation->set_rules("actual_price", "actual price", "required", array(
                'required' => 'actual price cannot be empty',
            ));
            $this->form_validation->set_rules("discount_price", "discount price", "required", array(
                'required' => 'discount price cannot be empty',
            ));
//            $this->form_validation->set_rules("exam_id", "exam id", "required", array(
//                'required' => 'exam id cannot be empty',
//            ));
            $this->form_validation->set_rules("exam_mode", "exam mode", "required", array(
                'required' => 'exam mode cannot be empty',
            ));
            $this->form_validation->set_rules("subject_wise", "subject wise", "required", array(
                'required' => 'subject wise cannot be empty',
            ));
            $this->form_validation->set_rules("status", "Status", "required", array(
                'required' => 'Status cannot be empty',
            ));
            $this->form_validation->set_rules("subjects_list[0]", "subjects list", "required", array(
                'required' => 'subjects list cannot be empty',
            ));
            $this->form_validation->set_rules("points[0]", "description points", "required", array(
                'required' => 'description points cannot be empty',
            ));
            $this->form_validation->set_rules("color", "color", "required", array(
                'required' => 'color cannot be empty',
            ));
            $this->form_validation->set_rules("course_id", "course", "required", array(
                'required' => 'course cannot be empty',
            ));
            if ($this->form_validation->run() == FALSE) {

            } else {
                $path = 'uploads/plans/';
                $image = $this->file_upload_with_specific_path('schedule_pdf', $path);
                $data = array(
                    'schedule_pdf' => $path . $image,
                    'plan_title' => $this->input->post('plan_title'),
                    'actual_price' => $this->input->post('actual_price'),
                    'discount_price' => $this->input->post('discount_price'),
                    'exam_id' => $this->input->post('exam_id'),
                    'exam_mode' => $this->input->post('exam_mode'),
                    'subject_wise' => $this->input->post('subject_wise'),
                    'status' => $this->input->post('status'),
                    'color' => $this->input->post('color'),
                    'course_id' => $this->input->post('course_id'),
                    'created_at' => time(),
                );
                $res = $this->{$this->model}->add($data, $this->input->post('subjects_list'), $this->input->post('points'));
                if ($res) {
                    $this->session->set_flashdata('success_message', '"Success!","Added Successfully!"');
                    redirect(base_url() . 'admin/plans');
                } else {
                    $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                    redirect(base_url() . 'admin/plans/add');
                }
            }
        }
        $this->data['courses_list'] = $this->course_model->get_active_data();
        $this->data['exams'] = $this->db->where('status', 'active')->get('exams')->result();
        $this->data['subjects'] = $this->Subject_model->get_active_data();
        $this->data['page_title'] = 'Add Plan';
        $this->admin_view('plans_add');
    }

    function update($id) {
        if ((isset($id)) && ($id != '')) {
            $this->data['details'] = $this->{$this->model}->get_details($id);

            if ($this->input->post('submit')) {
                $this->form_validation->set_rules("plan_title", "Plan Title", "required", array(
                    'required' => 'Plan Title cannot be empty',
                ));
                $this->form_validation->set_rules("actual_price", "actual price", "required", array(
                    'required' => 'actual price cannot be empty',
                ));
                $this->form_validation->set_rules("discount_price", "discount price", "required", array(
                    'required' => 'discount price cannot be empty',
                ));
                $this->form_validation->set_rules("exam_id", "exam id", "required", array(
                    'required' => 'exam id cannot be empty',
                ));
                $this->form_validation->set_rules("exam_mode", "exam mode", "required", array(
                    'required' => 'exam mode cannot be empty',
                ));
                $this->form_validation->set_rules("subject_wise", "subject wise", "required", array(
                    'required' => 'subject wise cannot be empty',
                ));
                $this->form_validation->set_rules("status", "Status", "required", array(
                    'required' => 'Status cannot be empty',
                ));
                $this->form_validation->set_rules("subjects_list[0]", "subjects list", "required", array(
                    'required' => 'subjects list cannot be empty',
                ));
                $this->form_validation->set_rules("points[0]", "description points", "required", array(
                    'required' => 'description points cannot be empty',
                ));
                $this->form_validation->set_rules("color", "color", "required", array(
                    'required' => 'color cannot be empty',
                ));
                $this->form_validation->set_rules("course_id", "course", "required", array(
                    'required' => 'course cannot be empty',
                ));
                if ($this->form_validation->run() == FALSE) {

                } else {
                    if ($_FILES['schedule_pdf']['name'] != '') {
                        $path = 'uploads/plans/';
                        $image = $this->file_upload_with_specific_path('schedule_pdf', $path);
                        $schedule_pdf = $path . $image;
                    } else {
                        $schedule_pdf = $this->data['details']->schedule_pdf;
                    }
                    $data = array(
                        'schedule_pdf' => $schedule_pdf,
                        'plan_title' => $this->input->post('plan_title'),
                        'actual_price' => $this->input->post('actual_price'),
                        'discount_price' => $this->input->post('discount_price'),
                        'exam_id' => $this->input->post('exam_id'),
                        'exam_mode' => $this->input->post('exam_mode'),
                        'subject_wise' => $this->input->post('subject_wise'),
                        'status' => $this->input->post('status'),
                        'color' => $this->input->post('color'),
                        'course_id' => $this->input->post('course_id'),
                        'updated_at' => time(),
                    );
                    $res = $this->{$this->model}->update($id, $data, $this->input->post('subjects_list'), $this->input->post('points'));
                    if ($res) {
                        $this->session->set_flashdata('success_message', '"Success!","Updated Successfully!"');
                        redirect(base_url() . 'admin/plans');
                    } else {
                        $this->session->set_flashdata('error_message', '"Error Occured!","Please try again later."');
                        redirect(base_url() . 'admin/plans/update/' . $id);
                    }
                }
            }
            $this->data['courses_list'] = $this->course_model->get_active_data();
            $this->data['exams'] = $this->db->where('status', 'active')->get('exams')->result();
            $this->data['subjects'] = $this->Subject_model->get_active_data();
            $this->data['page_title'] = 'Update Plan';
            $this->admin_view('plans_add');
        }
    }

    function view($id) {
        $this->data['details'] = $this->{$this->model}->get_details($id);
        $this->data['page_title'] = 'Plan Details';
        $this->admin_view('plans_view');
    }

    function delete($id) {
        if ((isset($id)) && ($id != '')) {
            if ($this->{$this->model}->delete($id)) {
                $this->session->set_flashdata('success_message', "'Deleted Successfully', 'Success'");
                redirect(base_url() . 'admin/plans');
            } else {
                $this->session->set_flashdata('error_message', "'Please Try Again', 'Error'");
                redirect(base_url() . 'admin/plans');
            }
        } else {
            redirect(base_url() . 'admin/plans');
        }
    }

}
