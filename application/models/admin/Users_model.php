<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

    private $table = "users";

    function get_details($id) {
        $this->db->where('id', $id);
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->row();
            return $result;
        }
    }

    function get_details_by_mobile_or_email($username) {
        $this->db->where("email = '$username' or phone_number = '$username'");
        $data = $this->db->get($this->table);
        if ($data->num_rows() > 0) {
            $result = $data->row();
            return $result;
        }
        return FALSE;
    }

    function add($data) {
        $this->db->set($data);
        $this->db->insert($this->table);
        if ($this->db->insert_id()) {
            return $this->db->insert_id();
        }
    }

    function update($id, $data) {
        $this->db->set($data);
        $this->db->where('id', $id);
        if ($this->db->update($this->table)) {
            return true;
        }
    }

}
